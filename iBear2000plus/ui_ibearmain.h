/********************************************************************************
** Form generated from reading UI file 'ibearmain.ui'
**
** Created by: Qt User Interface Compiler version 5.4.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_IBEARMAIN_H
#define UI_IBEARMAIN_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_IBearMain
{
public:
    QWidget *centralWidget;

    void setupUi(QMainWindow *IBearMain)
    {
        if (IBearMain->objectName().isEmpty())
            IBearMain->setObjectName(QStringLiteral("IBearMain"));
        IBearMain->resize(400, 300);
        centralWidget = new QWidget(IBearMain);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        IBearMain->setCentralWidget(centralWidget);

        retranslateUi(IBearMain);

        QMetaObject::connectSlotsByName(IBearMain);
    } // setupUi

    void retranslateUi(QMainWindow *IBearMain)
    {
        IBearMain->setWindowTitle(QApplication::translate("IBearMain", "IBearMain", 0));
    } // retranslateUi

};

namespace Ui {
    class IBearMain: public Ui_IBearMain {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_IBEARMAIN_H
