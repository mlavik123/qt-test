//
// Created by Jostein Eriksen on 07/04/16.
//

#ifndef IBEAR2000PLUS_MOVABLEBUTTON2_H
#define IBEAR2000PLUS_MOVABLEBUTTON2_H


#include <QPushButton>
#include <QMouseEvent>
#include "subject.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QColorDialog>
#include <iostream>
#include <src/data/ControlPoint.h>

class TransferFunction;
class ColorControlPoint;
class AlphaControlPoint;

class MovableButton2 : public QPushButton, public Subject {
public:
	explicit MovableButton2(QWidget *parent=0, TransferFunction* = 0);
	void setMovableBounds(QRect const &bounds);
	// const do not alter members
	const QRect& getMovableBounds() const { return mMovableBounds; };
	void disableYAxis() { mYAxisEnabled = !mYAxisEnabled; };
	void disableXAxis() { mXAxisEnabled = !mXAxisEnabled; };
	QPoint& newPos() { return mNewPos; }
	QColor& color() { return mNewColor; }
	void setColor(QColor color) { mNewColor = color; }
	void enableColorPicker() { mColorPickerEnabled = true; }
	void showColorPickerDialog();

	TransferFunction* mTf;
	ColorControlPoint* colorCP;
	AlphaControlPoint* alpha0CP;
	AlphaControlPoint* alpha1CP;
	AlphaControlPoint* alpha2CP;
	float mIsoValue;
	float mDiff = 0.005;

protected:
	void mouseMoveEvent(QMouseEvent * event) override;
	void mousePressEvent(QMouseEvent * event) override;
	void mouseReleaseEvent(QMouseEvent * event) override;

private:
	bool mMovable = false;
	bool mXAxisEnabled = true;
	bool mYAxisEnabled = true;
	QPoint mClickedPos;
	QRect mMovableBounds = QRect(0, 0, 0, 0);
	QPoint mNewPos;
	bool mColorPickerEnabled = false;
	QColor mNewColor;

};


#endif //IBEAR2000PLUS_MOVABLEBUTTON2_H
