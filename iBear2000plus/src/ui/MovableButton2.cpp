//
// Created by Jostein Eriksen on 07/04/16.
//

#include <iostream>
#include "MovableButton2.h"
#include "TransferFunction.h"
#include <QDebug>
#include "GlobalSettings.h"

void MovableButton2::setMovableBounds(QRect const &bounds)
{
	mMovableBounds = QRect(bounds.left(), bounds.top(), bounds.width() - width(), bounds.height() - height());
}

void MovableButton2::mouseMoveEvent(QMouseEvent *event)
{
	if(!(event->buttons() & Qt::LeftButton))
		return;

	// assume user always clicks the middle of the button???
	// find out where on the button the user clicked
	mNewPos.setX(pos().x() + (event->pos().x() - mClickedPos.x()));
	mNewPos.setY(pos().y() + (event->pos().y() - mClickedPos.y()));

	if(!mXAxisEnabled)
		mNewPos.setX(pos().x());
	else if(mNewPos.x() < mMovableBounds.left())
		mNewPos.setX(mMovableBounds.left());
	else if(mNewPos.x() + width() > mMovableBounds.width())
		// why on earth we do not need - width here, but we need it in height is beyond me... QT is strange
		mNewPos.setX(mMovableBounds.width() - width() / 2);

	if(!mYAxisEnabled)
		mNewPos.setY(pos().y());
	else if(mNewPos.y() < mMovableBounds.top())
		mNewPos.setY(mMovableBounds.top());
	else if(mNewPos.y() + height() > mMovableBounds.height())
		mNewPos.setY(mMovableBounds.height() - height());

	if(mNewPos.x() != pos().x() || mNewPos.y() != pos().y()) {
//		float diff = 0.002;
		float newIsoValue = mNewPos.x() / (float)mMovableBounds.width();
		if(newIsoValue + mDiff > 1 || newIsoValue - mDiff < 0)
			return;
		//if(newIsoValue != mIsoValue || ((MovableButton*) subject)->color().name() != mColor.name()) {
		mIsoValue = newIsoValue;
		std::cout << "newIsoValue: " << mIsoValue << std::endl;
		colorCP->mIsoValue = mIsoValue;
		colorCP->mColor = mNewColor;
		alpha0CP->mIsoValue = mIsoValue - mDiff;
		alpha1CP->mIsoValue = mIsoValue;
		alpha2CP->mIsoValue = mIsoValue + mDiff;
		mTf->generateTexture();
	}
//		notifyObservers();
}

void MovableButton2::showColorPickerDialog() {
	if(!mColorPickerEnabled)
	return;

	const QColor color = QColorDialog::getColor(mNewColor, this, "Select Color");

	if (color.isValid()) {
		setStyleSheet(this->styleSheet().replace(QRegExp("background-color: #[A-Fa-f0-9]{6}"), QString("background-color: ") + color.name()).toStdString().c_str());
		mNewColor = color;
		notifyObservers();
	}
};

void MovableButton2::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::RightButton) {
		showColorPickerDialog();
	}
	else {
		mClickedPos = event->pos();
		GlobalSettings::setFrameBufferResDivisor(GlobalSettings::frameres_low);
	}
}

void MovableButton2::mouseReleaseEvent(QMouseEvent *event)
{
    GlobalSettings::setFrameBufferResDivisor(1.0f);
    notifyObservers();
}

MovableButton2::MovableButton2(QWidget *parent, TransferFunction* tf) : QPushButton(parent)
{
	mIsoValue = 0.5;
	colorCP = new ColorControlPoint(255, 255, 255, 0.5);
	alpha0CP = new AlphaControlPoint(0, mIsoValue - mDiff);
	alpha1CP = new AlphaControlPoint(1, mIsoValue);
	alpha2CP = new AlphaControlPoint(0, mIsoValue + mDiff);

	mTf = tf;
	tf->addColorKnot(colorCP, false);
	tf->addAlphaKnot(alpha0CP, false);
	tf->addAlphaKnot(alpha1CP, false);
	tf->addAlphaKnot(alpha2CP, false);
}
