#include "UIFactory.h"

#include <QWidget>
#include <QGridLayout>
#include <QPushButton>
#include <QCheckBox>
#include <SceneManager.h>
#include <PlaneObject.h>
#include "SceneObject.h"
#include <iostream>
#include "Viewport.h"
#include "HistogramRenderView.h"
#include <string>
#include "ControlPoint.h"
#include <QDir>
#include "VolumeObject.h"
#include <QFileDialog>
#include <QString>
#include "VolumeRenderView.h"
#include "qxtspanslider.h"
#include "qvector.h"
#include <QLineEdit>
#include "resource_manager.h"
#include "TransferFunctionResource.h"
#include <QMessageBox>

int UIFactory::objNumber = 0;

void UIFactory::addPlane()
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();

    PlaneObject *plane = new PlaneObject((std::string("new_plane_") + std::to_string(objNumber)).c_str());
    SceneManager::instance()->addSceneObject(plane);
    objNumber ++;

    QCheckBox *chkSlice = new QCheckBox();
    chkSlice->setText("Is Slicer");
    chkSlice->setChecked(plane->mIsClippingPlane);

    QObject::connect(chkSlice, &QPushButton::clicked, [=](bool arg_checked) {
            plane->mIsClippingPlane = arg_checked;
            plane->notifyObservers();
        });

    lay->addWidget(chkSlice);

    window->setLayout(lay);

    window->show();
}

void UIFactory::selectActiveObject(View *arg_view)
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();


    for(SceneObject *obj : SceneManager::instance()->getSceneObjectsOfType<SceneObject>())
    {
        QPushButton *btn = new QPushButton();
        btn->setText(obj->getName().c_str());
        QObject::connect(btn, &QPushButton::clicked, [=]() {
                arg_view->setActiveObject(obj->getMemoryHash());
                btn->deleteLater();
                lay->deleteLater();
                window->deleteLater();
            });

        lay->addWidget(btn);
    }



    window->setLayout(lay);

    window->show();

}



void UIFactory::editObjects()
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();

int ii = 0;
    for(SceneObject *obj : SceneManager::instance()->getSceneObjectsOfType<SceneObject>())
    {
        QPushButton *btn = new QPushButton();
        btn->setText(obj->getName().c_str());

        QObject::connect(btn, &QPushButton::clicked, [=]() {

            PlaneObject *plane = dynamic_cast<PlaneObject*>(obj);
            if(plane != 0)
            {
                QCheckBox *chkSlice = new QCheckBox();
                chkSlice->setText("Is Slicer");
                chkSlice->setChecked(plane->mIsClippingPlane);

                QObject::connect(chkSlice, &QPushButton::clicked, [=](bool arg_checked) {
                        plane->mIsClippingPlane = arg_checked;
                        plane->notifyObservers();
                    });
                lay->addWidget(chkSlice,ii,1);
            }

        });

        lay->addWidget(btn,ii,0);
        ii++;
    }



    window->setLayout(lay);

    window->show();

}

void UIFactory::changeViewType(Viewport *arg_viewport)
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();

    Viewport::ViewportType vpTypes[3] = {Viewport::ViewportType::VOLUME, Viewport::ViewportType::SLICE, Viewport::ViewportType::HISTOGRAM};

    for(Viewport::ViewportType vpType : vpTypes)
    {
        std::string typeName;
        if(vpType == Viewport::ViewportType::VOLUME)
            typeName = "Volume renderer";
        else if(vpType == Viewport::ViewportType::SLICE)
            typeName = "Slice renderer";
        else
            typeName = "Histogram";

        QPushButton *btn = new QPushButton();
        btn->setText(typeName.c_str());
        QObject::connect(btn, &QPushButton::clicked, [=]() {
                arg_viewport->setViewportType(vpType);
                btn->deleteLater();
                lay->deleteLater();
                window->deleteLater();
            });

        lay->addWidget(btn);
    }



    window->setLayout(lay);

    window->show();
}


void UIFactory::selectActiveTF(View *arg_view)
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();


    QDir tfDir("data/TFPresets/");
    for(QFileInfo fileInfo : tfDir.entryInfoList())
    {
        QPushButton *btn = new QPushButton();
        btn->setText(fileInfo.fileName());
        QObject::connect(btn, &QPushButton::clicked, [=]() {
                TransferFunctionResource *res = ResourceManager<TransferFunctionResource>::instance()->getResource(fileInfo.fileName().toStdString().c_str());
                if(!res)
                {
                    res = ResourceManager<TransferFunctionResource>::instance()->addResource(new TransferFunctionResource(fileInfo.fileName().toStdString().c_str(), TransferFunction::newFromPreset(fileInfo.fileName().toStdString().c_str())));
                    res->getTransferFunction()->generateTexture();
                }
                arg_view->setTransferFunction(fileInfo.fileName().toStdString().c_str());
                res->getTransferFunction()->notifyObservers();
                btn->deleteLater();
                lay->deleteLater();
                window->deleteLater();
        });

        lay->addWidget(btn);
    }

    window->setLayout(lay);

    window->show();
}

void UIFactory::addAlphaKnot(TransferFunction *arg_tf)
{
    arg_tf->addAlphaKnot(new AlphaControlPoint(0.5f, 0.5f), true);
}

void UIFactory::addColourKnot(TransferFunction *arg_tf)
{
    ColorControlPoint *colPoint = new ColorControlPoint(255,0,0,0.5f, arg_tf);
    arg_tf->addColorKnot(colPoint, true);
    colPoint->mShowColourPicker = true;
}

void UIFactory::loadTFPreset(TransferFunction *arg_tf)
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();


    QDir tfDir("data/TFPresets/");
    for(QFileInfo fileInfo : tfDir.entryInfoList())
    {
        QPushButton *btn = new QPushButton();
        btn->setText(fileInfo.fileName());
        QObject::connect(btn, &QPushButton::clicked, [=]() {
                arg_tf->loadPreset(fileInfo.fileName().toStdString().c_str());
                btn->deleteLater();
                lay->deleteLater();
                window->deleteLater();
                arg_tf->generateTexture();
                arg_tf->notifyObservers();
            });

        lay->addWidget(btn);
    }

    window->setLayout(lay);

    window->show();
}

void UIFactory::saveTFPreset(TransferFunction *arg_tf)
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();


    QLineEdit *txtName = new QLineEdit(0);
    txtName->setText(QString::fromStdString(arg_tf->getName()));

    QPushButton *btn = new QPushButton();
    btn->setText("Save");
    QObject::connect(btn, &QPushButton::clicked, [=]() {
            arg_tf->savePreset(txtName->text().toStdString().c_str());
            btn->deleteLater();
            lay->deleteLater();
            window->deleteLater();
        });
    lay->addWidget(txtName);
    lay->addWidget(btn);

    window->setLayout(lay);

    window->show();
}

void UIFactory::loadVolume(VolumeRenderView *arg_view)
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();

    QDir tfDir("data/");
    for(QFileInfo fileInfo : tfDir.entryInfoList())
    {
        if(!fileInfo.fileName().contains(".dat"))
            continue;

        QPushButton *btn = new QPushButton();
        btn->setText(fileInfo.fileName());
        QObject::connect(btn, &QPushButton::clicked, [=]() {
                std::string strName = fileInfo.fileName().section(".",0,0).toStdString();
                std::string volObjID;
                SceneObject *tmpObj = SceneManager::instance()->getSceneObjectByName(strName.c_str());
                if(!tmpObj)
                {
                    VolumeObject *volObj = 0;
                    try
                      {
                        volObj = new VolumeObject(strName.c_str(), strName.c_str());
                      }
                      catch (std::bad_alloc& ba)
                      {
                        LOG_ERROR() << "bad_alloc when loading volume";
                        QMessageBox msgBox;
                        msgBox.setText("Not enough memory to load the volume! You have loaded too many volumes. Please restart the program. (sorry, we did not get time to implement a volume deletion menu..)");
                        msgBox.exec();
                        return;
                      }
                    volObjID = SceneManager::instance()->addSceneObject(volObj);
                }
                else
                    volObjID = tmpObj->getMemoryHash();
                arg_view->setVolumeObject(volObjID);
                btn->deleteLater();
                lay->deleteLater();
                window->deleteLater();
            });

        lay->addWidget(btn);
    }

    window->setLayout(lay);

    window->show();
}

void UIFactory::showVolumeRendererSettings(VolumeRenderView *arg_view)
{

    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();

    QLabel *lblGradientMagnitude = new QLabel();
    lblGradientMagnitude->setText("Gradient magnitude filter range");

    QxtSpanSlider *spanSlider = new QxtSpanSlider(0);
    spanSlider->setFocusPolicy(Qt::NoFocus);
    spanSlider->setOrientation(Qt::Horizontal);
    spanSlider->setRange(0,100);

    spanSlider->setUpperValue((int)(arg_view->getGradientFilterRange().y()*100.0));
    spanSlider->setLowerValue((int)(arg_view->getGradientFilterRange().x()*100.0));

    QObject::connect(spanSlider, &QxtSpanSlider::lowerValueChanged, [=](int val) {
            float fVal = ((float)val)/100.0;
            fVal = fVal*fVal;
            arg_view->setGradientFilterRange(QVector2D(fVal, arg_view->getGradientFilterRange().y()));
            arg_view->update();
        });
    QObject::connect(spanSlider, &QxtSpanSlider::upperValueChanged, [=](int val) {
            float fVal = ((float)val)/100.0;
            fVal = fVal*fVal;
            arg_view->setGradientFilterRange(QVector2D(arg_view->getGradientFilterRange().x(), fVal));
            arg_view->update();
        });

    QCheckBox *chkMaximumIntensityProjection = new QCheckBox();
    chkMaximumIntensityProjection->setText("Maximum intensity projection");
    chkMaximumIntensityProjection->setChecked(arg_view->getMaximumIntensityProjection());

    QObject::connect(chkMaximumIntensityProjection, &QPushButton::clicked, [=](bool arg_checked) {
            arg_view->setMaximumIntensityProjection(arg_checked);
            arg_view->update();
        });


    QLabel *lblQualitySettings = new QLabel(0);
    lblQualitySettings->setText("Quality settings");

    QPushButton *btnFastMode = new QPushButton();
    btnFastMode->setText("Fast mode");
    QObject::connect(btnFastMode, &QPushButton::clicked, [=]() {
            GlobalSettings::setRaymarchingSteps(350);
            arg_view->update();
        });
    QPushButton *btnNormalMode = new QPushButton();
    btnNormalMode->setText("Normal mode");
    QObject::connect(btnNormalMode, &QPushButton::clicked, [=]() {
            GlobalSettings::setRaymarchingSteps(500);
            arg_view->update();
        });
    QPushButton *btnHQMode = new QPushButton();
    btnHQMode->setText("HQ mode");
    QObject::connect(btnHQMode, &QPushButton::clicked, [=]() {
            GlobalSettings::setRaymarchingSteps(700);
            arg_view->update();
        });

    lay->addWidget(btnFastMode);
    lay->addWidget(btnNormalMode);
    lay->addWidget(btnHQMode);

    lay->addWidget(lblGradientMagnitude);
    lay->addWidget(spanSlider);
    lay->addWidget(chkMaximumIntensityProjection);
    window->setLayout(lay);
    window->show();
    //QObject::connect(chkSlice, &QPushButton::clicked, [=](bool arg_checked) {
    //        plane->mIsClippingPlane = arg_checked;
    //    });
}

void UIFactory::setVolume(View* arg_view)
{
    QWidget *window = new QWidget();

    QGridLayout *lay = new QGridLayout();


    for(VolumeObject *obj : SceneManager::instance()->getSceneObjectsOfType<VolumeObject>())
    {
        QPushButton *btn = new QPushButton();
        btn->setText(obj->getName().c_str());
        QObject::connect(btn, &QPushButton::clicked, [=]() {
                arg_view->setVolumeObject(obj->getMemoryHash());
                btn->deleteLater();
                lay->deleteLater();
                window->deleteLater();
            });

        lay->addWidget(btn);
    }
    window->setLayout(lay);

    window->show();
}
