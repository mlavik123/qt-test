//
// Created by Jostein Eriksen on 07/04/16.
//

#include <iostream>
#include "MovableButton.h"
#include "TransferFunction.h"
#include <QDebug>
#include "GlobalSettings.h"

void MovableButton::setMovableBounds(QRect const &bounds)
{
	mMovableBounds = QRect(bounds.left(), bounds.top(), bounds.width() - width(), bounds.height() - height());
}

void MovableButton::mouseMoveEvent(QMouseEvent *event)
{
	if(!(event->buttons() & Qt::LeftButton))
		return;

	// assume user always clicks the middle of the button???
	// find out where on the button the user clicked
	mNewPos.setX(pos().x() + (event->pos().x() - mClickedPos.x()));
	mNewPos.setY(pos().y() + (event->pos().y() - mClickedPos.y()));

	if(!mXAxisEnabled)
		mNewPos.setX(pos().x());
	else if(mNewPos.x() < mMovableBounds.left())
		mNewPos.setX(mMovableBounds.left());
	else if(mNewPos.x() + width() > mMovableBounds.width())
		// why on earth we do not need - width here, but we need it in height is beyond me... QT is strange
		mNewPos.setX(mMovableBounds.width() - width() / 2);

	if(!mYAxisEnabled)
		mNewPos.setY(pos().y());
	else if(mNewPos.y() < mMovableBounds.top())
		mNewPos.setY(mMovableBounds.top());
	else if(mNewPos.y() + height() > mMovableBounds.height())
		mNewPos.setY(mMovableBounds.height() - height());

	if(mNewPos.x() != pos().x() || mNewPos.y() != pos().y())
		notifyObservers();
}

void MovableButton::showColorPickerDialog() {
	if(!mColorPickerEnabled)
	return;

	const QColor color = QColorDialog::getColor(mNewColor, this, "Select Color");

	if (color.isValid()) {
		setStyleSheet(this->styleSheet().replace(QRegExp("background-color: #[A-Fa-f0-9]{6}"), QString("background-color: ") + color.name()).toStdString().c_str());
		mNewColor = color;
		notifyObservers();
	}
};

void MovableButton::mousePressEvent(QMouseEvent *event)
{
	if(event->button() == Qt::RightButton) {
		showColorPickerDialog();
	}
	else {
		mClickedPos = event->pos();
		GlobalSettings::setFrameBufferResDivisor(GlobalSettings::frameres_low);
	}
}

void MovableButton::mouseReleaseEvent(QMouseEvent *event)
{
    GlobalSettings::setFrameBufferResDivisor(1.0f);
    notifyObservers();
}

MovableButton::MovableButton(QWidget *parent) : QPushButton(parent)
{

}
