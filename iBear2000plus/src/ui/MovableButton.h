//
// Created by Jostein Eriksen on 07/04/16.
//

#ifndef IBEAR2000PLUS_MOVABLEBUTTON_H
#define IBEAR2000PLUS_MOVABLEBUTTON_H


#include <QPushButton>
#include <QMouseEvent>
#include "subject.h"
#include <QApplication>
#include <QDesktopWidget>
#include <QColorDialog>
#include <iostream>

class TransferFunction;
class ColorControlPoint;
class AlphaControlPoint;

class MovableButton : public QPushButton, public Subject {
public:
	explicit MovableButton(QWidget *parent=0);
	void setMovableBounds(QRect const &bounds);
	// const do not alter members
	const QRect& getMovableBounds() const { return mMovableBounds; };
	void disableYAxis() { mYAxisEnabled = !mYAxisEnabled; };
	void disableXAxis() { mXAxisEnabled = !mXAxisEnabled; };
	QPoint& newPos() { return mNewPos; }
	QColor& color() { return mNewColor; }
	void setColor(QColor color) { mNewColor = color; }
	void enableColorPicker() { mColorPickerEnabled = true; }
	void showColorPickerDialog();

protected:
	void mouseMoveEvent(QMouseEvent * event) override;
	void mousePressEvent(QMouseEvent * event) override;
	void mouseReleaseEvent(QMouseEvent * event) override;

private:
	bool mMovable = false;
	bool mXAxisEnabled = true;
	bool mYAxisEnabled = true;
	QPoint mClickedPos;
	QRect mMovableBounds = QRect(0, 0, 0, 0);
	QPoint mNewPos;
	bool mColorPickerEnabled = false;
	QColor mNewColor;
};


#endif //IBEAR2000PLUS_MOVABLEBUTTON_H
