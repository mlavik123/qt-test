#ifndef IBEAR2000PLUS_UIFACTORY_H
#define IBEAR2000PLUS_UIFACTORY_H

#include <View.h>
#include "TransferFunction.h"


class Viewport; // forward decl.
class HistogramRenderView;
class VolumeRenderView;

class UIFactory
{

public:
    static void addPlane();
    static void selectActiveObject(View *arg_view);
    static void editObjects();
    static void changeViewType(Viewport *arg_viewport);
    static void selectActiveTF(View *arg_view);
    static void addAlphaKnot(TransferFunction *arg_tf);
    static void addColourKnot(TransferFunction *arg_tf);
    static void loadTFPreset(TransferFunction *arg_tf);
    static void saveTFPreset(TransferFunction *arg_tf);
    static void loadVolume(VolumeRenderView *arg_view);
    static void showVolumeRendererSettings(VolumeRenderView *arg_view);
    static void setVolume(View* arg_view);

private:
    static int objNumber;

};


#endif //IBEAR2000PLUS_UIFACTORY_H
