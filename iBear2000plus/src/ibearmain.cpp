#include "ibearmain.h"
#include "ui_ibearmain.h"
#include "Viewport.h"
#include "TransferFunctionResource.h"
#include "TransferFunction.h"
#include "resource_manager.h"
#include <qgridlayout.h>
#include "SceneManager.h"
#include "PlaneObject.h"
#include "VolumeObject.h"
#include <string>
#include "observermanager.h"

IBearMain::IBearMain(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::IBearMain)
{


}

IBearMain::~IBearMain()
{
    delete ui;
}

void IBearMain::update()
{
    ObserverManager::getInstance()->update();
}

void IBearMain::closeEvent ( QCloseEvent * event )
{
    std::cout<< "CLOSING" << std::endl;
    mIsClosed = true;
    QWidget::closeEvent(event);
}

bool IBearMain::isClosed()
{
    return mIsClosed;
}

void IBearMain::initialise()
{
    ui->setupUi(this);

    ResourceManager<TransferFunctionResource>::create();
    SceneManager::create();

    ResourceManager<TransferFunctionResource>::instance()->addResource(new TransferFunctionResource("default", TransferFunction::newFromPreset("default")));

    // Create geometry
    PlaneObject *sliceRendererPlane = new PlaneObject("SliceRender");
    sliceRendererPlane->mIsClippingPlane = false;
    SceneManager::instance()->addSceneObject(sliceRendererPlane);
    //PlaneObject *slice1 = new PlaneObject("SliceObject");
    //slice1->mIsClippingPlane = true;
    //SceneManager::instance()->addSceneObject(slice1);

    std::string volObj = SceneManager::instance()->addSceneObject(new VolumeObject("manix-256x256x230", "manix-256x256x230"));


  // Main layout for ViewPorts:
    QGridLayout *layout = new QGridLayout();
    centralWidget()->setLayout (layout);

  // Add ViewPorts:
    Viewport *vp1 = new Viewport();
    Viewport *vp2 = new Viewport();
    Viewport *vp3 = new Viewport();
    Viewport *vp4 = new Viewport();

    layout->addWidget (vp1 , 0, 0);
    layout->addWidget (vp2 , 0, 1);
    layout->addWidget (vp3 , 1, 0);
    layout->addWidget (vp4 , 1, 1);

    vp1->setViewportType(Viewport::ViewportType::SLICE);
    vp2->setViewportType(Viewport::ViewportType::VOLUME);
    vp3->setViewportType(Viewport::ViewportType::HISTOGRAM);
    vp4->setViewportType(Viewport::ViewportType::VOLUME);

    this->showMaximized();
}
