//
// Created by Jostein Eriksen on 11/03/16.
//

#include "TransferFunction.h"
#include <assert.h>
#include <iostream>
#include <algorithm>
#include <qpainter.h>
#include <QTextStream>
#include <sstream>
#include <vector>
#include "ControlPoint.h"

ControlPoint ControlPointInst;

TransferFunction::~TransferFunction()
{

}

enum DESERIALIZE_STATE {
	COLORKNOT,
	ALPHAKNOT,
	NONE
};

TransferFunction* TransferFunction::newFromPreset(const char *preset)
{
	TransferFunction* tf = new TransferFunction();
	tf->loadPreset(preset);
	return tf;
}

void TransferFunction::savePreset(const char *preset_)
{
	const std::string preset = "data/TFPresets/" + std::string(preset_) + ".tfps";
	QFile file(preset.c_str());
	if (!file.open(QIODevice::ReadWrite|QFile::Truncate)) {
		std::cout << "Failed to read file: " << preset << std::endl;
		throw std::runtime_error("");
	}

	QTextStream stream(&file);
	stream << "[ColorKnots]" << endl;
	for(int i = 1; i < mColorKnots.size() -1; i++) {
		stream << mColorKnots[i]->mColor.red() << "," << mColorKnots[i]->mColor.green() << "," << mColorKnots[i]->mColor.blue() << "," << mColorKnots[i]->mIsoValue << endl;
	}

	stream << "[AlphaKnots]" << endl;
	for(int i = 1; i < mAlphaKnots.size() -1; i++) {
		stream << mAlphaKnots[i]->mAlpha << "," << mAlphaKnots[i]->mIsoValue << endl;
	}

	file.close();
}

void TransferFunction::loadPreset(const char *preset_)
{
	mClearButtons = true;
	const std::string preset = "data/TFPresets/" + std::string(preset_) + (std::string(preset_).find(".") != std::string::npos ? "" : ".tfps");
    resetKnots();

	QFile file(preset.c_str());
	file.open(QIODevice::ReadOnly);
	if(!file.isOpen()) {
		std::cout << "Failed to read file: " << preset << std::endl;
		throw std::runtime_error("");
	}

	DESERIALIZE_STATE state = NONE;
	QTextStream in(&file);

	while (!in.atEnd())
	{
		QString line = in.readLine();
		if( line.at(0) == '#')
			continue;
		else if(line == "[ColorKnots]") {
			state = COLORKNOT;
			continue;
		}
		else if (line == "[AlphaKnots]") {
			state = ALPHAKNOT;
			continue;
		}
		else if(line == "")
			continue;
		float isoValue;

		std::vector<std::string> vect;
		std::istringstream ss(line.toStdString());
		std::string token;

		while(std::getline(ss, token, ',')) {
			vect.push_back(token);
		}

		switch (state) {
			case COLORKNOT: {
				int r = std::stoi(vect[0]);//255;
				int g = std::stoi(vect[1]);//255;
				int b = std::stoi(vect[2]);//255;
				isoValue = std::stof(vect[3]);//vect.pop_back();
				vect.clear();

				addColorKnot(new ColorControlPoint(r, g, b, isoValue), false);
				break;
			}

			case ALPHAKNOT: {
				float alpha = std::stof(vect[0]);
				isoValue = std::stof(vect[1]);
				vect.clear();

				addAlphaKnot(new AlphaControlPoint(alpha, isoValue), false);
				break;
			}

			default:
				break;
		}
	}

	mName = std::string(preset_);

	file.close();
}

//void TransferFunction::doshit()
//{
//	glGenTextures(1, &mTexture);
//}

std::vector<ColorControlPoint*>* TransferFunction::getColorKnots()
{
    return &mColorKnots;
}

std::vector<AlphaControlPoint*>* TransferFunction::getAlphaKnots()
{
    return &mAlphaKnots;
}

TransferFunction::TransferFunction() : mTextureInitialized(false)
{
    resetKnots();
}

void TransferFunction::addAlphaKnot(AlphaControlPoint* point, bool generateTexture)
{
	point->mTf = this;
	AlphaControlPoint* p = mAlphaKnots.back();
	mAlphaKnots.pop_back();
	mAlphaKnots.push_back(point);
	mAlphaKnots.push_back(p);

	if(generateTexture)
		this->generateTexture();
}

void TransferFunction::addColorKnot(ColorControlPoint* point, bool generateTexture)
{
	point->mTf = this;
	ColorControlPoint* p = mColorKnots.back();
	mColorKnots.pop_back();
	mColorKnots.push_back(point);
	mColorKnots.push_back(p);

	if(generateTexture)
		this->generateTexture();
}

void TransferFunction::clearAlphaKnots()
{

}

void TransferFunction::clearCOlorKnots()
{

}

void TransferFunction::removeAlphaKnot(AlphaControlPoint * alphaKnot)
{
//	mAlphaKnots.erase(alphaKnot);
}

void TransferFunction::removeColorKnot(ColorControlPoint * colorKnot)
{

}

int interpolate(int startValue, int endValue, int stepNumber, int lastStepNumber)
{
	return (endValue - startValue) * stepNumber / lastStepNumber + startValue;
}
float interpolate(float startValue, float endValue, float stepNumber, float lastStepNumber)
{
	return (endValue - startValue) * stepNumber / lastStepNumber + startValue;
}

GLuint TransferFunction::getTexture()
{
	return mTexture;
}

void TransferFunction::initializeTexture()
{
	if(mTextureInitialized)
		return;

	glGenTextures(1, &mTexture);
	glBindTexture(GL_TEXTURE_2D, mTexture);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	//	 Multiply texture and object color (default setting)
	glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_BLEND);
    mTextureInitialized = true;
}

QImage* TransferFunction::getImage()
{
    return mImage;
}

QImage* TransferFunction::getImage2D()
{
	if(mImage2D == nullptr)
		generateTexture();

    return mImage2D;
}

void TransferFunction::generateTexture()
{
	if(mImage != nullptr)
		delete mImage;

	if(mImage2D != nullptr)
		delete mImage2D;

    mImage = new QImage(TEXTURE_WIDTH, TEXTURE_HEIGTH, QImage::Format_RGBA8888);//Format_ARGB32_Premultiplied);
    QPainter painter(mImage);

    mImage2D = new QImage(TEXTURE_WIDTH, TEXTURE_2D_HEIGHT, QImage::Format_RGBA8888);//Format_ARGB32_Premultiplied);
    QPainter painter2D(mImage2D);

	initializeTexture();

    std::vector<ColorControlPoint*> tmpColorKnots;
    for(ColorControlPoint *point : mColorKnots)
        tmpColorKnots.push_back(point);

    std::sort(tmpColorKnots.begin(), tmpColorKnots.end(), ControlPointInst);

	// get alpha knots
	// sort alpha knots
    for(uint ki = 0, i = 0, c = tmpColorKnots.size(); ki < c-1; ki++) {
        ColorControlPoint* point0 = tmpColorKnots[ki];
        ColorControlPoint* point1 = tmpColorKnots[ki+1];
		// find step count between these two points

		uint stepCount = uint(point1->mIsoValue * 256 - point0->mIsoValue * 256);
        for(int si = 0; si < stepCount; si++) {
			QColor color(
					interpolate(point0->mColor.red(), point1->mColor.red(), si, stepCount),
					interpolate(point0->mColor.green(), point1->mColor.green(), si, stepCount),
					interpolate(point0->mColor.blue(), point1->mColor.blue(), si, stepCount)
			);
			// r
			mTextureData[i*4] = color.red()/256.f;
			// g
			mTextureData[i*4+1] = color.green()/256.f;
			// b
			mTextureData[i*4+2] = color.blue()/256.f;
			// a
			painter.fillRect(i, 0, 1, 1, color);
            float a = 0;
            for(int y = 0; y < TEXTURE_2D_HEIGHT; y++) {
                //color.setAlpha(clamp(y - 50, 0, 255));
                color.setAlpha(255);
                a = clamp(y - 50, 1, 255)/255.0f;
                QColor outCol = color;
                outCol.setRed(outCol.red()*a);
                outCol.setGreen(outCol.green()*a);
                outCol.setBlue(outCol.blue()*a);
                painter2D.fillRect(i, y, 1, 1, outCol);
            }

//			mTextureData[i*4+3] = 1;
			i++;
		}
	}

    std::vector<AlphaControlPoint*> tmpAlphaKnots;
    for(AlphaControlPoint *point : mAlphaKnots)
        tmpAlphaKnots.push_back(point);

    std::sort(tmpAlphaKnots.begin(), tmpAlphaKnots.end(), ControlPointInst);

    for(uint ki = 0, i = 0, c = tmpAlphaKnots.size(); ki < c-1; ki++) {
        AlphaControlPoint* point0 = tmpAlphaKnots[ki];
        AlphaControlPoint* point1 = tmpAlphaKnots[ki+1];

		// find step count between these two points
		uint stepCount = uint(point1->mIsoValue * 256 - point0->mIsoValue * 256);
		for(int si = 0; si < stepCount; si++) {
			// a
			mTextureData[i*4+3] = interpolate((float)point0->mAlpha, (float)point1->mAlpha, (float)si, (float)stepCount);
			i++;
		}
	}

	glBindTexture(GL_TEXTURE_2D, mTexture);

	glTexImage2D(GL_TEXTURE_2D,
				 0, // level: Specifies the level-of-detail number. Level 0 is the base image level.
				 GL_RGBA,
				 255, // width
				 1, // height
				 0,
				 GL_RGBA,
				 GL_FLOAT,
                 mTextureData);

	glBindTexture(GL_TEXTURE_2D, 0);

    notifyObservers();
}

std::string TransferFunction::getName()
{
    return mName;
}

void TransferFunction::resetKnots()
{
    mAlphaKnots.clear();
    mColorKnots.clear();
    mAlphaKnots.push_back(new AlphaControlPoint(0, 0, this));
    mAlphaKnots.push_back(new AlphaControlPoint(0, 1, this));
    mColorKnots.push_back(new ColorControlPoint(0, 0, 0, 0, this));
    mColorKnots.push_back(new ColorControlPoint(0, 0, 0, 1, this));
}
