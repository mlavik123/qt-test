#ifndef IBEAR2000PLUS_CONTROLPOINT_H
#define IBEAR2000PLUS_CONTROLPOINT_H

#include "observer.h"
#include "MovableButton.h"
#include <assert.h>

class TransferFunction;

class ControlPoint : public IObserver {
public:
    bool operator()(ControlPoint* c1, ControlPoint* c2) { return (c1->mIsoValue < c2->mIsoValue); }
//	~ControlPoint();

public: // yeah yeah, i know
    float mIsoValue;
    TransferFunction* mTf = nullptr;
    void setIsoValue(float arg_value);

};

class AlphaControlPoint : public ControlPoint {
public:
    AlphaControlPoint(float alpha, float isoValue);
    AlphaControlPoint(float alpha, float isoValue, TransferFunction *tf) : AlphaControlPoint(alpha, isoValue) { mTf = tf; };

    void notify(Subject* subject);

    float mAlpha;
    void setAlphaValue(float arg_value);
};

class ColorControlPoint : public ControlPoint {
public:
    ColorControlPoint(int r, int g, int b, float isoValue);
    ColorControlPoint(int r, int g, int b, float isoValue, TransferFunction *tf) : ColorControlPoint(r, g, b, isoValue) { mTf = tf; };

    // TODO: change color somehow
    void notify(Subject* subject);

    QColor mColor;

    bool mShowColourPicker;
};

#endif
