#ifndef IBEAR2000PLUS_NOISETEXTURE1D_H
#define IBEAR2000PLUS_NOISETEXTURE1D_H

#include <vector>

#include "GLIncludes.h"
#include "debug.h"

#define TEXTURE_SIZE 1024

class NoiseTexture1D
{

private:
    float mTextureData[TEXTURE_SIZE*TEXTURE_SIZE];
    GLuint mTexture;
    bool mInitialised = false;

public:
    NoiseTexture1D();
    ~NoiseTexture1D();
    GLuint getTexture();
};


#endif
