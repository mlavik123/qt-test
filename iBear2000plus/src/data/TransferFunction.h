//
// Created by Jostein Eriksen on 11/03/16.
//

#ifndef IBEAR2000PLUS_TRANSFERFUNCTION_H
#define IBEAR2000PLUS_TRANSFERFUNCTION_H

#include "GLIncludes.h"
#include <vector>
#include <QColor>
#include <qimage.h>
#include "resource.h"
#include "subject.h"
#include "observer.h"
#include "ControlPoint.h"


class TransferFunction : public Subject
{
public:
	TransferFunction();
	~TransferFunction();
	void addColorKnot(ColorControlPoint*, bool = true);
	void addAlphaKnot(AlphaControlPoint*, bool = true);
	void removeColorKnot(ColorControlPoint*);
	void removeAlphaKnot(AlphaControlPoint*);
	void clearAlphaKnots();
	void clearCOlorKnots();

	QImage* getImage();
	QImage* getImage2D();
	GLuint getTexture();
    std::vector<ColorControlPoint*> *getColorKnots();
    std::vector<AlphaControlPoint*> *getAlphaKnots();
	void generateTexture();
	void savePreset(const char *);
	void loadPreset(const char *);
	static TransferFunction* newFromPreset(const char *);
	std::string getName();
	bool mClearButtons = false;

private:
	const int TEXTURE_WIDTH = 256;
	const int TEXTURE_HEIGTH = 1;
	const int TEXTURE_2D_HEIGHT = 256;
    QImage *mImage = nullptr;
    QImage *mImage2D = nullptr;
    float mTextureData[1024];
	bool mTextureInitialized;
	GLuint mTexture;
	std::string mName;
    void resetKnots();


	void initializeTexture();
	std::vector<ColorControlPoint*> mColorKnots;
	std::vector<AlphaControlPoint*> mAlphaKnots;


    int clamp(int n, int lower, int upper) {
        return std::max(lower, std::min(n, upper));
    }


};



#endif //IBEAR2000PLUS_TRANSFERFUNCTION_H
