#include "VolumeData.h"
#include <qfile.h>
#include <iostream>
#include <QVector2D>
#include <cmath>
#include <QTextStream>
#include <sstream>
#include "macro.h"

QVector4D gradientDirection(uint ix, uint iy, uint iz, VolumeData* data)
{

	// x, y, z
	int dim[] = {
			(int)data->getDimensions().x(),
			(int)data->getDimensions().y(),
			(int)data->getDimensions().z()
	};

	uint inputs[] = {
			ix, iy, iz
	};

	float results[3];

	ushort* dataPoints = data->getData();

	for(uint i = 0; i < 3; i++) {
		// +1, then -1
		for(int idy = 1; idy >= -1; idy -= 2) {
			int x = i == 0 && (int)inputs[i] + idy >= 0 && inputs[i] + idy < dim[i]? inputs[i] + idy: inputs[0];
			int y = i == 1 && (int)inputs[i] + idy >= 0 && inputs[i] + idy < dim[i]? inputs[i] + idy: inputs[1];
			int z = i == 2 && (int)inputs[i] + idy >= 0 && inputs[i] + idy < dim[i]? inputs[i] + idy: inputs[2];

			float dv = dataPoints[(uint)(x + y * dim[0] + z * dim[0] * dim[1])] / (float)USHORT_MAX;
			results[i] = idy == 1? dv: results[i] - dv;
		}
	}

	float magnitude = (float)(fabs(results[0]) + fabs(results[1]) + fabs(results[2])) / 3.f;

    QVector3D vec(results[0] / data->getDistance().x(), results[1] / data->getDistance().y(), results[2] / data->getDistance().z());
//	vec->normalize();
	return QVector4D(vec.x(), vec.y(), vec.z(), magnitude);
}

Gradient::Gradient(VolumeData & volumeData)
{
    mVolumeData = &volumeData;

	//ushort* data = volumeData.getData();
	QVector3D t = volumeData.getDimensions();
    mDimensions = QVector3D(t.x() / 2, t.y() / 2, t.z() / 2);
	if(fmod(mDimensions.x(), 2) > 0)
		mDimensions.setX(mDimensions.x() +1);
	if(fmod(mDimensions.y(), 2) > 0)
		mDimensions.setY(mDimensions.y() +1);
	if(fmod(mDimensions.z(), 2) > 0)
		mDimensions.setZ(mDimensions.z() +1);

    uint ab = (uint)(mDimensions.x() * mDimensions.y() * mDimensions.z() * 4);
	mGradient = new short[ab];

    short maxDir = 0;

	for(uint x = 0, xc = t.x(); x < xc; x+=2) {
		for(uint y = 0, yc = t.y(); y < yc; y+=2) {
			for(uint z = 0, zc = t.z(); z < zc; z+=2) {
				QVector4D direction = gradientDirection(x, y, z, &volumeData);
				uint index = (uint)(x / 2 + (y / 2) * (xc / 2) + (z / 2) * (xc / 2) * (yc / 2)) * 4;

                short mag = direction.w() * SHORT_MAX;
                if(mag > maxDir)
                    maxDir = mag;

				mGradient[index] = direction.x() * SHORT_MAX;
				mGradient[index+1] = direction.y() * SHORT_MAX;
				mGradient[index+2] = direction.z() * SHORT_MAX;
				mGradient[index+3] = mag;
			}
		}
	}

	for(int i = 3; i < (uint)(mDimensions.x() * mDimensions.y() * mDimensions.z() * 4); i+=4)
        mGradient[i] /= maxDir;

	return;
}

GLuint Gradient::getTexture()
{
    if(!mTextureIsInitialised)
        generateTexture();
    return mTexture;
}

void Gradient::generateTexture()
{
    glGenTextures(1, &mTexture);
    glBindTexture(GL_TEXTURE_3D, mTexture);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

    #if defined(_WIN32)
    PFNGLTEXIMAGE3DPROC glTexImage3D = (PFNGLTEXIMAGE3DPROC) wglGetProcAddress("glTexImage3D");
#endif
    glTexImage3D(GL_TEXTURE_3D, 0,
				 GL_RGBA,
                 mDimensions.x(), mDimensions.y(), mDimensions.z(), 0,
				 GL_RGBA,
				 GL_SHORT,
                 getTextureData());

    delete[] this->mGradient;

    mTextureIsInitialised = true;
}

Histogram::Histogram(uint* data, uint size, uint maxFreq)
{
    mData = data;
    mSize = size;
    mMaxFreq = maxFreq;
}

Histogram::~Histogram()
{
    if(mData != nullptr)
        delete mData;
}

const uint* Histogram::getData()
{
    return mData;
}

uint Histogram::getMaxFreq()
{
    return mMaxFreq;
}

uint Histogram::getSize()
{
    return mSize;
}

VolumeData::VolumeData()
{
    mDistance = QVector3D(1.0f,1.0f,1.0f);
}

VolumeData::~VolumeData()
{
    delete mGradient;
    delete mHistogram;
    delete[] mData;
}


QVector3D& VolumeData::getDimensions()
{
    return mDimensions;
}

ushort& VolumeData::getValue(uint x, uint y, uint z) const
{
	return mData[(uint)(x + y * mDimensions.x() + z * mDimensions.x() * mDimensions.z())];
}

ushort * VolumeData::getData() const
{
    return mData;
}

Histogram* VolumeData::getHistogram()
{
    return mHistogram;
}

bool VolumeData::readFile(const char * arg_filename, bool arg_logData)
{
    std::string fileName = std::string("data/") + std::string(arg_filename);
	QFile iniFile(QString::fromStdString(fileName + ".ini"));
	iniFile.open(QIODevice::ReadOnly);
	if(!iniFile.isOpen()) {
 		std::cout << "Failed to read file: " << iniFile.fileName().toStdString().c_str() << std::endl;
		throw std::runtime_error("");
	}

	// read the lines
	QTextStream in(&iniFile);

	while (!in.atEnd()) {
		QString line = in.readLine();

		std::vector<std::string> vect;
		std::istringstream ss(line.toStdString());
		std::string token;

		while(std::getline(ss, token, '=')) {
			vect.push_back(token);
		}

		if(vect[0] == "oldDat Spacing X")
			mDistance.setX(std::stof(vect[1]));
		else if(vect[0] == "oldDat Spacing Y")
			mDistance.setY(std::stof(vect[1]));
		else if(vect[0] == "oldDat Spacing Z")
			mDistance.setZ(std::stof(vect[1]));
	}
	iniFile.close();

	// normalize the distances
	if(mDistance.x() != 1) {
		float tmp = mDistance.x();
		mDistance.setX(mDistance.x() / tmp);
		mDistance.setY(mDistance.y() / tmp);
		mDistance.setZ(mDistance.z() / tmp);
	}

    QFile file(QString::fromStdString(fileName + ".dat"));
    file.open(QIODevice::ReadOnly);

    if(!file.isOpen()) {
        std::cout << "Failed to read file: " << file.fileName().toStdString().c_str() << std::endl;
        return false;
    }

    QByteArray byteArray = file.readAll();

    quint16 *tmpData;
    tmpData = (quint16*) byteArray.data();

    mDimensions = QVector3D(tmpData[0], tmpData[1], tmpData[2]);
    int uDimension = tmpData[0] * tmpData[1] * tmpData[2];

    mData  = new ushort[uDimension];

    float dataMinValue = mMaxValue;
    float dataMaxValue = 0.0f;


    uint* histData = new uint[(uint)mMaxValue+1]();
    uint maxFreq = 0;

    for(uint i = 0; i < uDimension; i++)
    {
        histData[tmpData[i+3]]++;
        if(histData[tmpData[i+3]] > maxFreq)
            maxFreq = histData[tmpData[i+3]];

        // normalize the value into a short
        mData[i] = (((float)tmpData[i+3])/(float)mMaxValue) * USHORT_MAX;
        if(mData[i] < dataMinValue)
            dataMinValue = mData[i];
        if(mData[i] > dataMaxValue)
            dataMaxValue = mData[i];
    }

    mHistogram = new Histogram(histData, mMaxValue, maxFreq);

    mGradient = new Gradient(*this);

    if(arg_logData)
    {
        std::cout << mDimensions.x() << std::endl;
        std::cout << mDimensions.y() << std::endl;
        std::cout << mDimensions.z() << std::endl;

        std::cout << dataMinValue << std::endl;
        std::cout << dataMaxValue << std::endl;
    }

    return true;

}

GLuint VolumeData::getTexture()
{
    if(!mTextureIsInitialised)
        initialiseTexture();
    mTextureIsInitialised = true;
    return mDataTexture;
}

void VolumeData::initialiseTexture()
{
    glGenTextures(1, &mDataTexture);
    glBindTexture(GL_TEXTURE_3D, mDataTexture);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_3D, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

#if defined(_WIN32)
    PFNGLTEXIMAGE3DPROC glTexImage3D = (PFNGLTEXIMAGE3DPROC) wglGetProcAddress("glTexImage3D");
#endif
    glTexImage3D(GL_TEXTURE_3D, 0,
                     GL_LUMINANCE,
                     this->getDimensions().x(), this->getDimensions().y(), this->getDimensions().z(), 0,
                     GL_RED,
                     GL_UNSIGNED_SHORT,
                     this->getData());
}
