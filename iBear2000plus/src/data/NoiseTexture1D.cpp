#include "NoiseTexture1D.h"


NoiseTexture1D::NoiseTexture1D()
{



}

NoiseTexture1D::~NoiseTexture1D()
{
}

GLuint NoiseTexture1D::getTexture()
{
    if(!mInitialised)
    {

        for(int i = 0; i < TEXTURE_SIZE*TEXTURE_SIZE; i++)
        {
            float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            //this->mTextureData[i] = (r + this->mTextureData[i])/2.0f;
            this->mTextureData[i] = r;
        }


        glGenTextures(1, &mTexture);
        glBindTexture(GL_TEXTURE_2D, mTexture);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        glTexEnvf(GL_TEXTURE_ENV,GL_TEXTURE_ENV_MODE, GL_BLEND);


        glBindTexture(GL_TEXTURE_2D, mTexture);

        //glTexImage1D(GL_TEXTURE_1D, 0, GL_RED, 255, 0, GL_RED, GL_FLOAT, mTextureData);
        //glTexImage1D(GL_TEXTURE_1D, 0, GL_RED, 255, 0, GL_RED, GL_FLOAT, mTextureData);

        glTexImage2D(GL_TEXTURE_2D,
                     0, // lod
                     GL_RGBA,
                     512,//255, // width
                     512,//255, // height
                     0,
                     GL_RGBA,
                     GL_FLOAT,
                     mTextureData);


        glBindTexture(GL_TEXTURE_2D, 0);

        mInitialised = true;
    }
    return this->mTexture;
}
