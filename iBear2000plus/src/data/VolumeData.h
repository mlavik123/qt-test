//
// Created by Jostein Eriksen on 03/03/16.
//

#ifndef CATERPILLAR_VOLUMEDATA_H
#define CATERPILLAR_VOLUMEDATA_H

#include <QVector3D>
#include <QVector4D>
#include <QOpenGLFunctions>

class VolumeData;

class Histogram {
public:
	Histogram(uint* data, uint size, uint maxFreq);
	~Histogram();
	const uint* getData();
	uint getSize();
	uint getMaxFreq();

private:
	uint *mData = nullptr;
	uint mSize = 0;
	uint mMaxFreq = 0;
};

class Gradient {
public:
	Gradient(VolumeData &);
	short* getTextureData() { return mGradient; };
	QPoint getTextureDimensions();
	GLuint getTexture();
	QVector3D mDimensions;

private:
	short* mGradient;
	GLuint mTexture;
	bool mTextureIsInitialised;
	VolumeData *mVolumeData;
	void generateTexture();
};

class VolumeData {
private:
	ushort *mData;
	Histogram *mHistogram = nullptr;
	Gradient *mGradient = nullptr;
	ushort mMaxValue = 4095;
	QVector3D mDimensions;
    QVector3D mDistance;
    void initialiseTexture();
    GLuint mDataTexture;
    bool mTextureIsInitialised;

public:
    VolumeData();
    ~VolumeData();

    bool readFile(const char* ag_filename, bool arg_logData = false);
	ushort* getData() const;
	ushort& getValue(uint x, uint y, uint z) const;
	Histogram* getHistogram();
    QVector3D& getDimensions();
	Gradient& getGradient() { return *mGradient; };
    QVector3D getDistance() { return mDistance; };
    GLuint getTexture();
};


#endif //CATERPILLAR_VOLUMEDATA_H
