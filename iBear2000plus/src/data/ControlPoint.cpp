#include <cmath>
#include "ControlPoint.h"
#include "TransferFunction.h"

void ControlPoint::setIsoValue(float arg_value)
{
    mIsoValue = arg_value;
}

void AlphaControlPoint::setAlphaValue(float arg_value)
{
    this->mAlpha = arg_value;
}

AlphaControlPoint::AlphaControlPoint(float alpha, float isoValue)
{
    assert(isoValue <= 1.f && isoValue >= 0 && alpha <= 1.f && alpha >= 0);
    mIsoValue = isoValue;
    mAlpha = alpha;
}


void AlphaControlPoint::notify(Subject* subject)
{
	float newNotLogVal = (((MovableButton*) subject)->getMovableBounds().height() - ((MovableButton*) subject)->newPos().y()) / (float)((MovableButton*) subject)->getMovableBounds().height();
	// WTF is this monstrosity???
	float newAlpha = ((pow(21, newNotLogVal) - 1) / 20);
    float newIsoValue = ((MovableButton*) subject)->newPos().x() / (float)((MovableButton*) subject)->getMovableBounds().width();
    //if(newAlpha != mAlpha || newIsoValue != mIsoValue) {
        mIsoValue = newIsoValue;
        mAlpha = newAlpha;
        mTf->generateTexture();
    //}
}

void ColorControlPoint::notify(Subject* subject)
{
    float newIsoValue = ((MovableButton*) subject)->newPos().x() / (float)((MovableButton*) subject)->getMovableBounds().width();
    //if(newIsoValue != mIsoValue || ((MovableButton*) subject)->color().name() != mColor.name()) {
        mIsoValue = newIsoValue;
        mColor = ((MovableButton*) subject)->color();
        mTf->generateTexture();
    //}
}

ColorControlPoint::ColorControlPoint(int r, int g, int b, float isoValue)
{
    mShowColourPicker = false;
    assert(isoValue <= 1.f && isoValue >= 0);
    assert(r <= 255 && r >= 0);
    assert(g <= 255 && g >= 0);
    assert(b <= 255 && b >= 0);
    mIsoValue = isoValue;
    mColor.setRgb(r, g, b, 1);
}
