//
// Created by Jostein Eriksen on 12/03/16.
//

#ifndef IBEAR2000PLUS_HISTOGRAMRENDERVIEW_H
#define IBEAR2000PLUS_HISTOGRAMRENDERVIEW_H

#include "View.h"
#include "VolumeData.h"
#include <QKeyEvent>
#include <QResizeEvent>
#include <qpushbutton.h>
#include <qlabel.h>
#include "MovableButton.h"
#include "MovableButton2.h"
#include "TransferFunction.h"
#include "subject.h"
#include <iostream>
#include <QDebug>
#include "UIFactory.h"
#include <vector>

// temporary class
enum BUTTON_MODE {
	NORMAL,
	VALUE_SWEEPER
};
class HistogramRenderView : public QWidget, public View {
	Q_OBJECT
public:
	HistogramRenderView(QWidget *parent);
	void notify(Subject* subject = nullptr);
	void initializeValueSweeperTF();
	void initializeNormalTF();
	void clearButtons();
	void fixButtons();
	~HistogramRenderView();


protected:
	void paintEvent(QPaintEvent *event);
//	void mouseMoveEvent(QMouseEvent * event);
	void resizeEvent(QResizeEvent * event);
    void mousePressEvent(QMouseEvent *event);

private:
	Histogram* mHistogram = nullptr;
	QLabel* mImageLabel;
	QLabel* mHistogramLabel;
	QImage* mHistogramImage;
	std::vector<MovableButton2*> mSingleButtons;
    std::vector<MovableButton*> mColourButtons;
    std::vector<MovableButton*> mAlphaButtons;
	BUTTON_MODE mButtonMode = NORMAL;

    void resizeTF();

public slots:
    void callbackSetActiveTF();
    void callbackChangeViewType();
    void callbackAddColourKnot();
    void callbackAddAlphaKnot();
    void callbackLoadPreset();
    void callbackSavePreset();
    void callbackSwipeMode();
};


#endif //IBEAR2000PLUS_HISTOGRAMRENDERVIEW_H
