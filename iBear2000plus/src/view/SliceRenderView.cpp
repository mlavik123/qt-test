#include "SliceRenderView.h"
#include "resource_manager.h"
#include "TransferFunctionResource.h"
#include <stdio.h>
#include <stdlib.h>
#include "VolumeObject.h"

#include <QKeyEvent>
#include <QApplication>
#include <QOpenGLShader>
#include <QOpenGLTexture>
#include <QOpenGLFunctions>
#include <QtOpenGL/QGLFormat>

#include "qpiemenu.h"
#include "GlobalSettings.h"
#include "SceneManager.h"
#include "VolumeRenderView.h"
#include "PlaneObject.h"
#include "macro.h"

using namespace std;

SliceRenderView::SliceRenderView(QWidget *parent)
    : QOpenGLWidget(parent)
{
    this->setActiveObject(SceneManager::instance()->getSceneObjectOfType<PlaneObject>()->getMemoryHash());
	mTransformation = &SceneManager::instance()->getSceneObjectByID(mActiveObject)->getTransform();

    setVolumeObject(SceneManager::instance()->getSceneObjectOfType<VolumeObject>()->getMemoryHash());

    mTimer = new QTimer(this);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(timerTimeout()));
}

SliceRenderView::~SliceRenderView()
{
	// What does this do???
	makeCurrent();
}


void SliceRenderView::updateView()
{

}

void SliceRenderView::initializeGL()
{
    setTransferFunction("default");

	initializeOpenGLFunctions();
	printf("OpenGL version supported by this platform (%s): \n", glGetString(GL_VERSION));

	initBuffers();

	mTransformation->translate(QVector3D(0.f, 0.f, -1.f));

	glEnable(GL_DEPTH_TEST);
	glCullFace(GL_FRONT);
	glEnable(GL_CULL_FACE);

	glEnable (GL_BLEND);
	glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glClearColor(0.7f, 0.7f, 0.7f, 1.0f);

	mShader = std::make_shared<QOpenGLShaderProgram>(nullptr);
	mShader->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/Slice.vs.glsl");
	mShader->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/Slice.fs.glsl");
	mShader->bindAttributeLocation("position", PROGRAM_VERTEX_ATTRIBUTE);
//	mShader->bindAttributeLocation("texCoord", PROGRAM_TEXCOORD_ATTRIBUTE);
	mShader->link();
	mShader->bind();

	mShader->setUniformValue("datatexture", 0);
	mShader->setUniformValue("transferTexture", 1);

}

void SliceRenderView::resizeGL(int w, int h)
{
}

void SliceRenderView::paintGL()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mVolume = (VolumeObject*)SceneManager::instance()->getSceneObjectByID(mVolumeObject);

    TransferFunctionResource* res = ResourceManager<TransferFunctionResource>::instance()->getResource(mTransferFunction.c_str());
    if(!res)
        return;
    TransferFunction *tf = res->getTransferFunction();

    // hack: do not store pointer to Transform between frames
    mTransformation = &SceneManager::instance()->getSceneObjectByID(mActiveObject)->getTransform();

	// Enable the shader program
	mShader->bind();
	// position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3,	GL_FLOAT, GL_FALSE, 0, 0);

	mCombined.setToIdentity();

    mCombined *=  mRotation * mTransformation->getTranslationMatrix();
//	qDebug() << "Scale in slice" << mVolume->getTransform().getScale() << endl;

	mShader->setUniformValue("translationCombinedMatrix", mTransformation->getModelMatrix() * mVolume->getTransform().getScaleMatrix());
	mShader->setUniformValue("textureSize", mVolumeData->getDimensions());

	// Bind the buffers
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIbo);

	glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_3D, mVolume->getVolumeData().getTexture());
    //glBindTexture(GL_TEXTURE_3D, mVolume->getVolumeData().getGradient().getTexture());
//	glBindTexture(GL_TEXTURE_3D, mGradientTexture);

	glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, tf->getTexture());

	// Draw the elements on the GPU
	glDrawElements(
			GL_TRIANGLES,				// the type of primitive to produce
			3 * 2,	// the number of indices
			GL_UNSIGNED_INT,			// the type of the indices
			0);							// offset of the first index

	// Disable the "position" vertex attribute (not necessary but recommended)
	glDisableVertexAttribArray(0);

	// Disable the shader program (not necessary but recommended)
	glUseProgram(0);
}


void SliceRenderView::initBuffers()
{
	const uint vertexCount = 12;
	const uint triangleCount = 2;
	static const GLfloat coords[vertexCount] = {
			-1.f,  1.f, 0.0f,
			 1.f,  1.f, 0.0f,
			-1.f, -1.f, 0.0f,
			 1.f, -1.f, 0.0f
	};
	// move the plane in place so you see the hand
	mTransformation->translate(QVector3D(0.f, 0.f, 0.05f*20));

	// Generate a VBO as in the previous tutorial
	glGenBuffers(1, &mVbo);
	glBindBuffer(GL_ARRAY_BUFFER, mVbo);
	glBufferData(GL_ARRAY_BUFFER,
				 vertexCount * sizeof(GLfloat),
				 coords,
				 GL_STATIC_DRAW);

	// Create an array of indices representing the triangles
	unsigned int indices[3 * triangleCount] = {
			0, 1, 2,	// first triangle
			2, 1, 3};	// second triangle

	// Create a buffer
	glGenBuffers(1, &mIbo);

	// Set it as a buffer for indices
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIbo);

	// Set the data for the current IBO
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
				 3 * triangleCount * sizeof(unsigned int),
				 indices,
				 GL_STATIC_DRAW);
}

void SliceRenderView::mouseMoveEvent(QMouseEvent * event)
{
    if(event->buttons() == Qt::LeftButton)
    {
            QMatrix4x4 rotX;
            QMatrix4x4 rotY;

            float xAngle = (mClickedPos.x() - event->pos().x()) * 0.05f;
            float yAngle = (mClickedPos.y() - event->pos().y()) * 0.05f;
            mClickedPos = event->pos();

            rotY.rotate(yAngle, QVector3D(1.f, 0.f, 0.f));
            rotX.rotate(xAngle, QVector3D(0.f, 1.f, 0.f));

            mTransformation->roatate(yAngle, QVector3D(1.f, 0.f, 0.f));
            mTransformation->roatate(xAngle, QVector3D(0.f, 1.f, 0.f));
            mRotation = mRotation * rotY * rotX;
    }


	this->update();
}

void SliceRenderView::mousePressEvent(QMouseEvent * event)
{
    GlobalSettings::setFrameBufferResDivisor(GlobalSettings::frameres_low);
	mClickedPos = event->pos();
    mMouseEvent = event;

    if(event->button() == Qt::MouseButton::RightButton)
    {
        QPieMenu *pieMenu = new QPieMenu(this);
            pieMenu->addAction("Add knot", QIcon("data/icons/edit_object.png"), this, SLOT(callbackAddKnot()));
            pieMenu->addAction("Active object", QIcon("data/icons/select_object.png"), this, SLOT(callbackChangePlane()));
            pieMenu->addAction("Set highlight position", QIcon("data/icons/highlight.png"), this, SLOT(callbackSetHightlight()));
            pieMenu->addAction("Set volume", QIcon("data/icons/select_volume.png"), this, SLOT(callbackSetVolume()));
            pieMenu->addAction("Set transfer function", QIcon("data/icons/tf.png"), this, SLOT(callbackSetActiveTF()));

            pieMenu->showMenu();
    }
}

void SliceRenderView::mouseReleaseEvent(QMouseEvent *event)
{
    GlobalSettings::setFrameBufferResDivisor(1.0f);
    SceneManager::instance()->getSceneObjectByID(this->mActiveObject)->notifyObservers();
}

void SliceRenderView::wheelEvent(QWheelEvent *event)
{
    GlobalSettings::setFrameBufferResDivisor(GlobalSettings::frameres_low);
    Transform &transform = SceneManager::instance()->getSceneObjectByID(this->mActiveObject)->getTransform();
    transform.translate(QVector3D(0.0f,0.0f,0.001f*(float)event->delta()));


    mTimer->start(300);
}

void SliceRenderView::keyPressEvent(QKeyEvent *event)
{

	if(event->key() == Qt::Key_Up) {
		mTransformation->translate(QVector3D(0.f, 0.05f, 0.f));
	}
	else if(event->key() == Qt::Key_Right) {
		mTransformation->translate(QVector3D(0.05f, 0.f, 0.f));
	}
	else if(event->key() == Qt::Key_Left) {
		mTransformation->translate(QVector3D(-0.05f, 0.f, 0.f));
	}
	else if(event->key() == Qt::Key_Down) {
		mTransformation->translate(QVector3D(0.f, -0.05f, 0.f));
	}

	if(event->key() == Qt::Key_W){
		mTransformation->translate(QVector3D(0.f, 0.f, -0.05f));
	}
	else if(event->key() == Qt::Key_S) {
		mTransformation->translate(QVector3D(0.f, 0.f, 0.05f));
	}

	this->update();
}


QVector3D SliceRenderView::getSliceToVolumeCoordinate(int arg_x, int arg_y)
{
    PlaneObject *plane = (PlaneObject*)SceneManager::instance()->getSceneObjectByID(this->mActiveObject);

    float x = (float)arg_x / (float)this->width() - 0.5f;
    float y = (float)arg_y / (float)this->height() - 0.5f;
    y=-y;

    QVector3D scale = plane->getTransform().getScale();
    QVector3D invScale = QVector3D(1.0f/scale.x(), 1.0f/scale.y(), 1.0f/scale.z());
    QVector3D invScale2 = QVector3D(1.0f/2.0f*scale.x(), 1.0f/2.0f*scale.y(), 1.0f/2.0f*scale.z());
    QVector4D vec(x, y, 0.0f, 1.0f);              // TODO: remove hard-coded scaling
    vec =  (plane->getTransform().getPosition()*invScale2 +   (plane->getTransform().getRotation() * vec)*invScale);

    vec = vec + QVector3D(0.5f,0.5f,0.5f);
    return QVector3D(vec.x(), vec.y(), vec.z());
}

// TODO: refactor this method to use short instead of float
float SliceRenderView::getValueAtMousePos(int arg_x, int arg_y)
{
    QVector3D vec = getSliceToVolumeCoordinate(arg_x, arg_y);

    VolumeObject *volObj = (VolumeObject*)SceneManager::instance()->getSceneObjectByID(this->mVolumeObject);
    ushort *d = volObj->getVolumeData().getData();
    QVector3D dim = volObj->getVolumeData().getDimensions();

    int ix = (int)(vec.x()*dim.x());
    int iy = (int)(vec.y()*dim.y());
    int iz = (int)(vec.z()*dim.z());

    int dimx = (int)dim.x();
    int dimy = (int)dim.y();
    int dimz = (int)dim.z();

    uint i = (uint)(ix + iy*dimx + iz*dimx*dimy);
    float intensity;
    if(i > dimx*dimy*dimz)
        LOG_ERROR() << "Trying to fetch value in data set width index higher than max index";
    else
        intensity = ((float)d[i])/USHORT_MAX; // ??
    return intensity;
}

void SliceRenderView::timerTimeout()
{
    GlobalSettings::setFrameBufferResDivisor(1.0f);
    mTimer->stop();
    SceneManager::instance()->getSceneObjectByID(this->mActiveObject)->notifyObservers();
}




void SliceRenderView::callbackAddKnot()
{

    float d = this->getValueAtMousePos(mClickedPos.x(), mClickedPos.y());
    if(d > 0.0f && d < 1.0f)
        LOG_INFO() << "adding knot at value: " << d;
    else
    {
        LOG_WARNING() << "cannot add knot at value: " << d;
        return;
    }

    tf->addAlphaKnot(new AlphaControlPoint(0.5f,d));
    tf->addAlphaKnot(new AlphaControlPoint(0.0f,std::max(d-0.02f, 0.0f)));
    tf->addAlphaKnot(new AlphaControlPoint(0.0f,d+std::min(0.02f, 1.0f)));
}

void SliceRenderView::callbackSetHightlight()
{
    VolumeObject *volObj = (VolumeObject*)SceneManager::instance()->getSceneObjectByID(this->mVolumeObject);
    volObj->setHightlightPos(this->getSliceToVolumeCoordinate(mClickedPos.x(), mClickedPos.y()));
}

void SliceRenderView::callbackChangePlane()
{
    UIFactory::selectActiveObject(this);
}

void SliceRenderView::callbackSetVolume()
{
    UIFactory::setVolume(this);
}

void SliceRenderView::callbackSetActiveTF()
{
    UIFactory::selectActiveTF(this);
}

