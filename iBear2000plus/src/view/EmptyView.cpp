#include "EmptyView.h"
#include <QMouseEvent>

EmptyView::EmptyView(QWidget *parent)
{

}

EmptyView::~EmptyView()
{

}

void EmptyView::mousePressEvent(QMouseEvent * event)
{
    if(event->button() == Qt::MouseButton::RightButton)
    {
        QPieMenu *pieMenu = new QPieMenu(this);
            pieMenu->addAction("Change view", QIcon("data/icons/zoom.png"), this, SLOT(callbackChangeViewType()));
            pieMenu->showMenu();
    }
}
