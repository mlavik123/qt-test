#ifndef CATERPILLAR_VIEWPORT_H
#define CATERPILLAR_VIEWPORT_H

#include <QWidget>
#include <QGridLayout>
#include <iostream>
#include "VolumeRenderView.h"
#include "View.h"
#include "SliceRenderView.h"
#include "HistogramRenderView.h"
#include "debug.h"

class Viewport : public QWidget {
	Q_OBJECT

public:
    enum ViewportType {
        SLICE,
        HISTOGRAM,
        VOLUME,
        EMPTY
    };

private:
    Viewport::ViewportType mType;

    QGridLayout *layout;

public:
	Viewport(QWidget* parent = 0, const char *name = 0);
	~Viewport();

    void setViewportType(Viewport::ViewportType arg_type);


};


#endif //CATERPILLAR_VIEWPORT_H
