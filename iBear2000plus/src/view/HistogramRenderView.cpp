#include <qpainter.h>
#include <iostream>
#include <qgridlayout.h>
#include <qtoolbutton.h>
#include <sstream>
#include <src/scene/SceneManager.h>
#include <src/scene/VolumeObject.h>
#include <src/ui/MovableButton2.h>
#include "HistogramRenderView.h"
#include "resource_manager.h"
#include "TransferFunctionResource.h"

HistogramRenderView::HistogramRenderView(QWidget *parent)
{

	VolumeData* data;
	data = &SceneManager::instance()->getSceneObjectOfType<VolumeObject>()->getVolumeData();
//	data.readFile("data/manix-256x256x230.dat", true);
	mHistogram = data->getHistogram();

	mImageLabel = new QLabel(this);
	mHistogramLabel = new QLabel(this);

    TransferFunctionResource* tfr = ResourceManager<TransferFunctionResource>::instance()->getResource("default");
    this->setTransferFunction(tfr->getResourceName());
    TransferFunction *tf = tfr->getTransferFunction();
    tf->registerObserver(this);

	// create the histogram
	const uint histBucketSize = 5;
	const uint histYScale = 500;

	const uint histWidth = mHistogram->getSize() / histBucketSize;
	const uint histHeight = mHistogram->getMaxFreq() / histYScale;

	qreal barWidth = histWidth / (qreal)(mHistogram->getSize());
	qreal barUnit = histHeight / (qreal)mHistogram->getMaxFreq();

	mHistogramImage = new QImage(histWidth, histHeight, QImage::Format_RGBA8888);//Format_ARGB32_Premultiplied);
	QPainter painter(mHistogramImage);

	const uint* histData = mHistogram->getData();

	for (int i = 0, c = mHistogram->getSize(); i < c; i+=histBucketSize) {
		uint currentHeight = 0;
		for(int y = 0; y < histBucketSize; y++) {
			currentHeight += histData[i+y];
		}

		qreal h = log10(currentHeight) / log10(mHistogram->getMaxFreq()) * histHeight;
		painter.fillRect(barWidth * i, histHeight - h, 1, h, QColor(255, 255, 255, 75));
	}

	//initializeValueSweeperTF();
}
void HistogramRenderView::clearButtons()
{
	for(uint i = 0; i < mSingleButtons.size(); i++) {
		delete mSingleButtons[i];
	}
	for(uint i = 0; i < mColourButtons.size(); i++) {
		delete mColourButtons[i];
	}
	for(uint i = 0; i < mAlphaButtons.size(); i++) {
		delete mAlphaButtons[i];
	}
	mColourButtons.clear();
	mAlphaButtons.clear();
	mSingleButtons.clear();
}
void HistogramRenderView::initializeNormalTF()
{
	mButtonMode = NORMAL;
	TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();
	// start with an empty tf
	tf->loadPreset("default");

	// clear the buttons
	clearButtons();

	tf->generateTexture();
	resizeTF();
}
void HistogramRenderView::initializeValueSweeperTF()
{
	mButtonMode = VALUE_SWEEPER;
	TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();
	// start with an empty tf
	tf->loadPreset("empty");
	tf->mClearButtons = false;

	clearButtons();

	// delete all other buttons

	for(uint i = 0; i < 1; i++) {
		MovableButton2* button = 0;
		// if button is not made, make it
		button = new MovableButton2(this, tf);
		button->enableColorPicker();
		button->disableYAxis();
		button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
		std::string style = "QPushButton::indicator { width: 15px; height: 35px; } QPushButton { background-color: #ffffff; border: 1px solid #676767; border-radius: 5px;  width: 10px; height: 30px; }";
		button->setStyleSheet(style.c_str());
		button->setColor(Qt::white);
		button->show();

		mSingleButtons.push_back(button);
	}

	tf->generateTexture();
	resizeTF();
}



// rearange all buttons when view is resized
void HistogramRenderView::notify(Subject* subject)
{
    TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();

	if(tf->mClearButtons){
		tf->mClearButtons = false;
		clearButtons();
	}

	fixButtons();
    mImageLabel->setPalette(QPalette());
    mImageLabel->setPixmap(QPixmap::fromImage(*tf->getImage2D()).scaled(this->width(), this->height(), Qt::IgnoreAspectRatio));
    mImageLabel->setFixedHeight(this->height());
    mImageLabel->setFixedWidth(this->width());
}

HistogramRenderView::~HistogramRenderView()
{

}

void HistogramRenderView::resizeEvent(QResizeEvent *event)
{
//	std::cout << "width is now: " << this->width() << std::endl;
    notify();
	resizeTF();
}

void HistogramRenderView::paintEvent(QPaintEvent *event)
{
}

void HistogramRenderView::fixButtons()
{
	// new version; make x buttons that you can drag left && right && change color on
	// this button will change tf values for 3 alpha knots & 1 color knots
//	TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();
	QRect bounds(0, 0, this->width(), this->height());

	switch (mButtonMode) {
		case VALUE_SWEEPER:
			for(uint i = 0; i < mSingleButtons.size(); i++) {
				MovableButton2* button = mSingleButtons[i];
				button->setMovableBounds(bounds);
				button->move(button->mIsoValue  * (this->width() - button->width()), this->height() - button->height());
			}
			break;

		case NORMAL:
		default:
			TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();

			std::vector<ColorControlPoint*> *colorKnots = tf->getColorKnots();
			std::vector<AlphaControlPoint*> *alphaKnots = tf->getAlphaKnots();

			QRect bounds(0, 0, this->width(), this->height());

			for(uint i = 1; i < colorKnots->size()-1; i++) {
				ColorControlPoint* colourPoint = colorKnots->at(i);
				MovableButton* button = 0;
				if(i-1 < mColourButtons.size())
					button = mColourButtons.at(i-1);
				// if button is not made, make it
				if(button == 0) {
					button = new MovableButton(this);
					button->enableColorPicker();
					button->disableYAxis();
					button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
					std::string style = "QPushButton::indicator { width: 15px; height: 35px; } QPushButton { background-color: " + colorKnots->at(i)->mColor.name().toStdString() + "; border: 1px solid #676767; border-radius: 5px;  width: 10px; height: 30px; }";
					button->setStyleSheet(style.c_str());
					button->setColor(colorKnots->at(i)->mColor);

					button->registerObserver(colourPoint);
					//colorKnots->at(i)->mButton = button;
					button->show();
					mColourButtons.push_back(button);
				}
				button->setMovableBounds(bounds);
				button->move(colorKnots->at(i)->mIsoValue * (this->width() - button->width()), this->height() - button->height());
				if(colourPoint->mShowColourPicker)
				{
					button->showColorPickerDialog();
					colourPoint->mShowColourPicker = false;
				}

			}

			// show all the alpha control points
			for(uint i = 1; i < alphaKnots->size()-1; i++) {
				MovableButton* button = 0;
				if(i-1 < mAlphaButtons.size())
					button = mAlphaButtons.at(i-1);
				// if button is not made, make it
				if(button == 0) {
					button = new MovableButton(this);
					button->setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
					button->setStyleSheet("QPushButton::indicator { width: 10px; height: 10px; } QPushButton { border: 1px solid #DFDFF5; background-color: #9CBFDE; border-radius: 6px;  width: 10px; height: 10px; }");
					button->registerObserver(alphaKnots->at(i));
					button->show();
					mAlphaButtons.push_back(button);
				}
				button->setMovableBounds(bounds);
				qreal h = log10(alphaKnots->at(i)->mAlpha * 20 + 1) / log10(21) * height();
				button->move(alphaKnots->at(i)->mIsoValue * (this->width() - button->width()), this->height() - h);
			}
			break;
	}
}

void HistogramRenderView::resizeTF()
{
	mHistogramLabel->setPixmap(QPixmap::fromImage(*mHistogramImage).scaled(this->width(), this->height(), Qt::IgnoreAspectRatio));
	mHistogramLabel->setFixedHeight(this->height());
	mHistogramLabel->setFixedWidth(this->width());
    // else this is another type of event
	fixButtons();
}




void HistogramRenderView::mousePressEvent(QMouseEvent *eventPress)
{
    if(eventPress->button() == Qt::MouseButton::RightButton)
    {
        QPieMenu *pieMenu = new QPieMenu(this);
            pieMenu->addAction("Change view", QIcon("data/icons/zoom.png"), this, SLOT(callbackChangeViewType()));
            pieMenu->addAction("Active transfer function", QIcon("data/icons/tf.png"), this, SLOT(callbackSetActiveTF()));
            pieMenu->addAction("Add colour knot", QIcon("data/icons/colour_knot.png"), this, SLOT(callbackAddColourKnot()));
            pieMenu->addAction("Add alpha knot", QIcon("data/icons/alpha_knot.png"), this, SLOT(callbackAddAlphaKnot()));
            pieMenu->addAction("Load preset", QIcon("data/icons/load.png"), this, SLOT(callbackLoadPreset()));
            pieMenu->addAction("Save preset", QIcon("data/icons/save.png"), this, SLOT(callbackSavePreset()));
            pieMenu->addAction("Swipe mode", QIcon("data/icons/swipe.png"), this, SLOT(callbackSwipeMode()));

            pieMenu->showMenu();
    }
}





void HistogramRenderView::callbackSetActiveTF()
{
    UIFactory::selectActiveTF(this);
}

void HistogramRenderView::callbackChangeViewType()
{
    UIFactory::changeViewType(mViewport);
}

void HistogramRenderView::callbackAddColourKnot()
{
    TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();
    UIFactory::addColourKnot(tf);
}

void HistogramRenderView::callbackAddAlphaKnot()
{
    TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();
    UIFactory::addAlphaKnot(tf);
}

void HistogramRenderView::callbackLoadPreset()
{
    TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();
    UIFactory::loadTFPreset(tf);
}

void HistogramRenderView::callbackSavePreset()
{
    TransferFunction* tf = ResourceManager<TransferFunctionResource>::instance()->getResource(this->getTransferFunction().c_str())->getTransferFunction();
    UIFactory::saveTFPreset(tf);
}

void HistogramRenderView::callbackSwipeMode()
{
    if(this->mButtonMode == NORMAL)
        this->initializeValueSweeperTF();
    else
        this->initializeNormalTF();
}
