#ifndef VIEW_H
#define VIEW_H

#include <QWidget>
#include <string>
#include "observer.h"
#include "subject.h"
#include "qpiemenu.h"


class Viewport; // forward declaration

class View :public IObserver
{

protected:
    std::string mActiveObject;
    Viewport* mViewport;
    std::string mVolumeObject;
    std::string mTransferFunction;

public:
    View() {}
    virtual ~View() {}

    virtual void updateView() {}

    virtual void notify(Subject *arg_subject) override;

    virtual void setActiveObject(std::string arg_obj);
    virtual void setVolumeObject(std::string arg_obj);
    void setViewport(Viewport* arg_viewport);

    void setTransferFunction(std::string arg_tf);
    std::string getTransferFunction();

};

#endif // VIEW_H
