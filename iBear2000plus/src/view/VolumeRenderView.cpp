#include "VolumeRenderView.h"

#include <QKeyEvent>
#include <QApplication>
#include <QOpenGLShader>
#include <QOpenGLTexture>
#include <QOpenGLShaderProgram>

#include <iostream>
#include <qwindow.h>

#include "SceneManager.h"
#include "SceneObject.h"
#include "PlaneObject.h"
#include "VolumeObject.h"

#include <qvector.h>
#include <vector>
#include "qpiemenu.h"
#include "GlobalSettings.h"
#include <QCursor>
#include "Viewport.h"
#include "resource_manager.h"
#include "TransferFunctionResource.h"


#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_TEXCOORD_ATTRIBUTE 1

VolumeRenderView::VolumeRenderView(QWidget *parent)
    : QOpenGLWidget(parent)
{
    mTransform = new Transform(0);
    mVolumeTransform = new Transform(0);
    SceneManager::instance()->registerObserver(this);

    mTimer = new QTimer(this);
    connect(mTimer, SIGNAL(timeout()), this, SLOT(timerTimeout()));
    mShowGeometry = true;

    setVolumeObject(SceneManager::instance()->getSceneObjectOfType<VolumeObject>()->getMemoryHash());
    setGradientFilterRange(QVector2D(0.0,1.0));
}

/**********/

VolumeRenderView::~VolumeRenderView()
{

}

/**********/

void VolumeRenderView::updateView()
{
    mNotified = true;
    update();
}

/**********/

void VolumeRenderView::notify(Subject *arg_subject)
{

    SceneManager *smgr = (SceneManager*)arg_subject;
    if(smgr)
        subscribeToSceneObjects();
    this->updateView();
}

void VolumeRenderView::subscribeToSceneObjects()
{
    for(PlaneObject *plane : SceneManager::instance()->getSceneObjectsOfType<PlaneObject>())
        plane->registerObserver(this);
}

/**********/

void VolumeRenderView::initializeGL()
{
    initializeOpenGLFunctions();

    this->setActiveObject(SceneManager::instance()->getSceneObjectOfType<VolumeObject>()->getMemoryHash());

    makeObject();

    glEnable(GL_DEPTH_TEST);
    glCullFace(GL_FRONT);
    glEnable(GL_CULL_FACE);
    glEnable (GL_BLEND);
    glBlendFunc (GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    mProgram = std::make_shared<QOpenGLShaderProgram>(nullptr);
    mProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/volrend_vert.vsh.glsl");
    mProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/volrend_frag.fsh.glsl");
    mProgram->bindAttributeLocation("vertex", PROGRAM_VERTEX_ATTRIBUTE);
    mProgram->bindAttributeLocation("texCoord", PROGRAM_TEXCOORD_ATTRIBUTE);
    mProgram->link();
    mProgram->bind();

    mGeometryProgram = std::make_shared<QOpenGLShaderProgram>(nullptr);
    mGeometryProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, ":/shaders/volrend_plane_vert.vsh.glsl");
    mGeometryProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, ":/shaders/volrend_plane_frag.fsh.glsl");
    mGeometryProgram->bindAttributeLocation("vertex", PROGRAM_VERTEX_ATTRIBUTE);
    mGeometryProgram->bindAttributeLocation("texCoord", PROGRAM_TEXCOORD_ATTRIBUTE);
    mGeometryProgram->link();

    mProgram->setUniformValue("datatexture", 0);
    mProgram->setUniformValue("tftexture",1);
    mProgram->setUniformValue("noisetexture",2);
    mProgram->setUniformValue("gradientdir",3);


    Transform &transform = SceneManager::instance()->getSceneObjectByID(this->mActiveObject)->getTransform();
    transform.translate(QVector3D(0.0f,0.0f,-6.0f));

    // todo!!
    setTransferFunction("default");



}

/**********/

void VolumeRenderView::resizeGL(int w, int h)
{


    mWidth = w * this->devicePixelRatio();
    mHeight = h * this->devicePixelRatio();
    mAspectRatio = mWidth/mHeight;
    //mAspectRatio = 1.0;
    QVector3D screenRes = QVector3D(mWidth, mHeight, fov);
    mProgram->setUniformValue("screenRes", screenRes);
    mProgram->setUniformValue("aspectRatio", mAspectRatio);

    QMatrix4x4 mat;
    mat.perspective(fov,mAspectRatio,0.2f,15.0f);
    mProgram->setUniformValue("projmat", mat);
}

/**********/

void VolumeRenderView::paintGL()
{
    // temporary fix: it seems taht Qt automatically updates the view, on some computers
    if(!mNotified)
        GlobalSettings::setFrameBufferResDivisor(1.0f);
    mNotified = false;

    TransferFunctionResource* res = ResourceManager<TransferFunctionResource>::instance()->getResource(mTransferFunction.c_str());
    if(!res)
        return;
    TransferFunction *tf = res->getTransferFunction();

    VolumeObject *volumeObject = (VolumeObject*)SceneManager::instance()->getSceneObjectByID(mVolumeObject);
    if(!volumeObject)
        return;

    GLuint dataTexture = volumeObject->getVolumeData().getTexture();

    int w = this->width() * this->devicePixelRatio();
    int h = this->height() * this->devicePixelRatio();

    w = w / GlobalSettings::getFrameBufferResDivisor();
    h = h / GlobalSettings::getFrameBufferResDivisor();

    texture_fbo = new QGLFramebufferObject(w, h);



    glViewport(0, 0, texture_fbo->size().width(), texture_fbo->size().height());

    QVector3D screenRes = QVector3D(texture_fbo->size().width(), texture_fbo->size().height(), fov);
    mProgram->setUniformValue("screenRes", screenRes);


    GLint drawFboId = 0, readFboId = 0;
    glGetIntegerv(GL_DRAW_FRAMEBUFFER_BINDING, &drawFboId);
    glGetIntegerv(GL_READ_FRAMEBUFFER_BINDING, &readFboId);



    texture_fbo->bind();

    glClearColor(0.7f, 0.7f, 0.7f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mProgram->setUniformValue("steps", GlobalSettings::getRaymarchingSteps());
    mProgram->setUniformValue("gradientMagRange", getGradientFilterRange());
    mProgram->setUniformValue("maximumIntensityProjection", getMaximumIntensityProjection());

    /***/
    /***** Render geometry *****/
    if(this->mShowGeometry)
    {
        mGeometryProgram->bind();
        mPlaneVbo.bind();
        glDisable(GL_CULL_FACE);

        mGeometryProgram->enableAttributeArray(PROGRAM_VERTEX_ATTRIBUTE);
        mGeometryProgram->enableAttributeArray(PROGRAM_TEXCOORD_ATTRIBUTE);
        mGeometryProgram->setAttributeBuffer(PROGRAM_VERTEX_ATTRIBUTE, GL_FLOAT, 0, 3, 0);

        for(PlaneObject *plane : SceneManager::instance()->getSceneObjectsOfType<PlaneObject>())
        {
            mGeometryProgram->setUniformValue("isClippingPlane", plane->mIsClippingPlane);

            QMatrix4x4 mat;
            mat.perspective(fov,mAspectRatio,0.2f,15.0f);
            mGeometryProgram->setUniformValue("projmat", mat);
            SceneObject *a = new SceneObject("a");
            Transform trans(a);
            trans.mRotation = plane->getTransform().mRotation;
            trans.translate(mVolumeTransform->mPosition);
            mGeometryProgram->setUniformValue("matrix",  mVolumeTransform->getModelMatrix() * plane->getTransform().getModelMatrix());
            delete a;
            glDrawArrays(GL_TRIANGLE_FAN, 0, 4);

        }
    }


    /***/
    /***** Render volume *****/
    mProgram->bind();
    mVbo.bind();
    glEnable(GL_CULL_FACE);
    glEnable(GL_DEPTH);
    glDepthFunc(GL_ALWAYS);

    mProgram->setUniformValue("matrix", mVolumeTransform->getModelMatrix());

    QMatrix4x4 mat;
    mat = mVolumeTransform->getModelMatrix().inverted();
    mProgram->setUniformValue("invmat", mat);
    mProgram->setUniformValue("scale", mVolumeTransform->getScale());
    mProgram->setUniformValue("highlightPos", volumeObject->getHighlightPos());

    mProgram->enableAttributeArray(PROGRAM_VERTEX_ATTRIBUTE);
    mProgram->enableAttributeArray(PROGRAM_TEXCOORD_ATTRIBUTE);
    mProgram->setAttributeBuffer(PROGRAM_VERTEX_ATTRIBUTE, GL_FLOAT, 0, 3, 6 * sizeof(GLfloat));
    mProgram->setAttributeBuffer(PROGRAM_TEXCOORD_ATTRIBUTE, GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));


    // Clipping planes
    std::vector<PlaneObject*> planes = SceneManager::instance()->getSceneObjectsOfType<PlaneObject>();
    if(!this->mShowGeometry)
        mProgram->setUniformValue("numPlanes", 0);
    else
    {
        mProgram->setUniformValue("numPlanes", (int)planes.size());
        for(int i = 0; i < planes.size(); i++)
        {
            std::string strPlane = std::string("planes[") + std::to_string(i) + std::string("]");
            PlaneObject *plane = planes[i];
            mProgram->setUniformValue((strPlane + std::string(".position")).c_str(), plane->getTransform().getPosition());
            mProgram->setUniformValue((strPlane + std::string(".normal")).c_str(), plane->getTransform().forward());
            mProgram->setUniformValue((strPlane + std::string(".clipping")).c_str(), plane->mIsClippingPlane);

            QVector3D n = plane->getTransform().forward();
            QVector3D t = plane->getTransform().right();
            QVector3D b = QVector3D::crossProduct(n,t);
            QMatrix4x4 tbn = QMatrix4x4(t.x(),b.x(),n.x(),0.0f,
                                        t.y(),b.y(),n.y(),0.0f,
                                        t.z(),b.z(),n.z(),0.0f,
                                         0.0f,0.0f,0.0f, 1.0f);
            mProgram->setUniformValue((strPlane + std::string(".tbn")).c_str(), tbn.inverted());
        }
    }


    for (int i = 0; i < 6; ++i)
    {
        glActiveTexture(GL_TEXTURE0);
           glBindTexture(GL_TEXTURE_3D, dataTexture);
        glActiveTexture(GL_TEXTURE1);
           glBindTexture(GL_TEXTURE_2D, tf->getTexture());
        glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, mNoiseTexture.getTexture());
        glActiveTexture(GL_TEXTURE3);
            glBindTexture(GL_TEXTURE_3D, volumeObject->getVolumeData().getGradient().getTexture());
        glDrawArrays(GL_TRIANGLE_FAN, i * 4, 4);
    }
    texture_fbo->release();



    glViewport(0, 0, this->width()* this->devicePixelRatio(), this->height()* this->devicePixelRatio());
    glBindFramebuffer(GL_FRAMEBUFFER, drawFboId);
    glUseProgram(0); // use default rendering pipeline: not "good", but doesn't matter much for rendering a plane
    glDisable(GL_CULL_FACE);

    glClearColor(0.0f, 0.0f, 0.7f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// Clear Screen And Depth Buffer
    glLoadIdentity();									// Reset The Current Modelview Matrix

    glActiveTexture(GL_TEXTURE0);
    glEnable(GL_TEXTURE_2D);
    glBindTexture (GL_TEXTURE_2D, texture_fbo->texture());

    glActiveTexture(GL_TEXTURE1);
    glDisable(GL_TEXTURE_2D);
    glActiveTexture(GL_TEXTURE2);
    glDisable(GL_TEXTURE_2D);

    glScalef(2.0f,2.0f,2.0f);
    glTranslatef(-0.5f,-0.5f,0.0f);


    glBegin (GL_QUADS);
    glTexCoord2f (0.0, 0.0);
    glVertex3f (0.0, 0.0, 0.0);
    glTexCoord2f (1.0, 0.0);
    glVertex3f (1.0, 0.0, 0.0);
    glTexCoord2f (1.0, 1.0);
    glVertex3f (1.0, 1.0, 0.0);
    glTexCoord2f (0.0, 1.0);
    glVertex3f (0.0, 1.0, 0.0);
    glEnd ();

    mProgram->bind(); // bind our own shader program

    delete(texture_fbo);

    return;

}

/**********/

void VolumeRenderView::makeObject()
{
    /* ***** Box ***** */
    static const GLfloat cubeCoords[6][4][3] =
    {
        { {1.0f, -1.0f, -1.0f}, {-1.0f, -1.0f, -1.0f}, {-1.0f, 1.0f, -1.0f}, {1.0f, 1.0f, -1.0f} }, // front
                { {1.0f, 1.0f, -1.0f}, {-1.0f, 1.0f, -1.0f}, {-1.0f, 1.0f, 1.0f}, {1.0f, 1.0f, 1.0f} }, // top
                { {-1.0f, -1.0f, 1.0f}, {1.0f, -1.0f, 1.0f}, {1.0f, 1.0f, 1.0f}, {-1.0f, 1.0f, 1.0f} }, // back
                { {1.0f, -1.0f, 1.0f}, {-1.0f, -1.0f, 1.0f}, {-1.0f, -1.0f, -1.0f}, {1.0f, -1.0f, -1.0f} }, // bottom
                { {1.0f, -1.0f, 1.0f}, {1.0f, -1.0f, -1.0f}, {1.0f, 1.0f, -1.0f}, {1.0f, 1.0f, 1.0f} }, // left
                { {-1.0f, -1.0f, -1.0f}, {-1.0f, -1.0f, 1.0f}, {-1.0f, 1.0f, 1.0f}, {-1.0f, 1.0f, -1.0f} } // right
    };



    QVector<GLfloat> cubeVertData;
    for (int i = 0; i < 6; ++i)
    {
        for (int j = 0; j < 4; ++j)
        {
            // vertex positions
            cubeVertData.append(cubeCoords[i][j][0]);
            cubeVertData.append(cubeCoords[i][j][1]);
            cubeVertData.append(cubeCoords[i][j][2]);
            // texture coordinates
            cubeVertData.append(cubeCoords[i][j][0]);
            cubeVertData.append(cubeCoords[i][j][1]);
            cubeVertData.append(cubeCoords[i][j][2]);
        }
    }

    mVbo.create();
    mVbo.bind();
    mVbo.allocate(cubeVertData.constData(), cubeVertData.count() * sizeof(GLfloat));


    /* ***** Plane ***** */
    static const GLfloat planeCoords[4][3] =
    {
        {1.0f, -1.0f, 0.0f}, {-1.0f, -1.0f, 0.0f}, {-1.0f, 1.0f, 0.0f}, {1.0f, 1.0f, 0.0f} // front
    };
    QVector<GLfloat> planeVertData;
    for (int j = 0; j < 4; ++j)
    {
        // vertex position
        planeVertData.append(planeCoords[j][0]);
        planeVertData.append(planeCoords[j][1]);
        planeVertData.append(planeCoords[j][2]);
    }

    mPlaneVbo.create();
    mPlaneVbo.bind();
    mPlaneVbo.allocate(planeVertData.constData(), planeVertData.count() * sizeof(GLfloat));
}

/**********/

/* **************************************
 *             Input Events
 * **************************************
*/


void VolumeRenderView::keyPressEvent(QKeyEvent *event)
{

    if (event->key() == Qt::Key_Escape)
    {
        event->accept();
        qApp->quit();
    }

    if(event->key() == Qt::Key_Right || event->key() == Qt::Key_Left)
    {
        QVector4D tmp = QVector4D(0.0f,1.0f,0.0f, 0.0f);
        if(event->key() == Qt::Key_Right)
            tmp = -tmp;
        mTransform->roatate(7.0f, QVector3D(tmp));
    }

    else if(event->key() == Qt::Key_Down || event->key() == Qt::Key_Up)
    {
        mTransform->roatate(7.0f, mTransform->right() * (event->key() == Qt::Key_Down ? -1.0f : 1.0f));
    }


    if(event->key() == Qt::Key_W)
        mTransform->translate(QVector3D(0.0f,0.0f,0.2f));
    else if(event->key() == Qt::Key_S)
        mTransform->translate(QVector3D(0.0f,0.0f,-0.2f));

   this->updateView();
}

/**********/

void VolumeRenderView::mousePressEvent(QMouseEvent *eventPress)
{
    if(eventPress->button() == Qt::MouseButton::RightButton)
    {
        // Prevent long waiting time during GUI rendering
        GlobalSettings::setFrameBufferResDivisor(GlobalSettings::frameres_low);

        QPieMenu *pieMenu = new QPieMenu(this);
            pieMenu->addAction("Change view", QIcon("data/icons/zoom.png"), this, SLOT(callbackChangeViewType()));
            pieMenu->addAction("Show/Hide geometry", QIcon("data/icons/show_geometry.png"), this, SLOT(callbackShowHideGeometry()));
            pieMenu->addAction("Add plane", QIcon("data/icons/add_plane.png"), this, SLOT(callbackAddPlane()));
            pieMenu->addAction("Active object", QIcon("data/icons/select_object.png"), this, SLOT(callbackSetActiveObject()));
            pieMenu->addAction("Edit objects", QIcon("data/icons/edit_object.png"), this, SLOT(callbackEditObjects()));
            pieMenu->addAction("Load volume", QIcon("data/icons/load.png"), this, SLOT(callbackLoadVolume()));
            pieMenu->addAction("Rendering settings", QIcon("data/icons/settings.png"), this, SLOT(callbackShowSettings()));
            pieMenu->addAction("Set volume", QIcon("data/icons/select_volume.png"), this, SLOT(callbackSetVolume()));
            pieMenu->addAction("Set transfer function", QIcon("data/icons/tf.png"), this, SLOT(callbackSetActiveTF()));

            pieMenu->showMenu();
    }
    else if(eventPress->button() == Qt::MouseButton::LeftButton)
    {
        QVector2D newPos = QVector2D(QCursor::pos().x(), QCursor::pos().y());
        mMousePos = newPos;

        GlobalSettings::setFrameBufferResDivisor(GlobalSettings::frameres_low);
    }
}

/**********/

void VolumeRenderView::mouseReleaseEvent(QMouseEvent *eventPress)
{
    if(eventPress->button() == Qt::MouseButton::LeftButton)
    {
        GlobalSettings::setFrameBufferResDivisor(1.0f);
        updateView();
    }
}

void VolumeRenderView::wheelEvent(QWheelEvent *event)
{
    GlobalSettings::setFrameBufferResDivisor(GlobalSettings::frameres_low);

    if(event->modifiers() & Qt::ControlModifier)
        mTransform->translate(QVector3D(0.0f,0.001f*(float)event->delta(),0.0f));
    else if(event->modifiers() & Qt::ShiftModifier)
        mTransform->translate(QVector3D(0.001f*(float)event->delta(),0.0f,0.0f));
    else
        mTransform->translate(QVector3D(0.0f,0.0f,0.001f*(float)event->delta()));
    updateView();
    mTimer->start(300);
}

void VolumeRenderView::mouseMoveEvent(QMouseEvent * event)
{
    QPoint cursorPos = QCursor::pos();

    QVector2D newPos = QVector2D(cursorPos.x(), cursorPos.y());
    QVector2D rotAngle = -(mMousePos - newPos)* 0.05f;
    mMousePos = newPos;

    if(event->buttons() == Qt::LeftButton)
    {

        mTransform->roatate(rotAngle.y(), QVector3D(1.f, 0.f, 0.f));
        mTransform->roatate(rotAngle.x(), QVector3D(0.f, 1.f, 0.f));

        QRect widgetRect = this->geometry();
        QPoint tr = this->mapToGlobal(widgetRect.topRight());
        QPoint br = this->mapToGlobal(widgetRect.bottomRight());
        QPoint tl = this->mapToGlobal(widgetRect.topLeft());
        QPoint bl = this->mapToGlobal(widgetRect.bottomLeft());


        QPoint newCursorPos;
        bool teleported = true;
        if(cursorPos.x() > tr.x())
            newCursorPos = (QPoint(tl.x(),cursorPos.y()));
        else if(cursorPos.x() < tl.x())
            newCursorPos = (QPoint(tr.x(),cursorPos.y()));
        else if(cursorPos.y() > bl.y())
            newCursorPos = (QPoint(cursorPos.x(),tl.y()));
        else if(cursorPos.y() < tl.y())
            newCursorPos = (QPoint(cursorPos.x(),bl.y()));
        else
            teleported = false;

        if(teleported)
        {
            QCursor::setPos(newCursorPos);
            cursorPos = newCursorPos;
            newPos = QVector2D(cursorPos.x(), cursorPos.y());
            mMousePos = newPos;
        }

        this->updateView();
    }

}

void VolumeRenderView::timerTimeout()
{
    GlobalSettings::setFrameBufferResDivisor(1.0f);
    mTimer->stop();
    updateView();
}

void VolumeRenderView::setActiveObject(std::string arg_obj)
{ // This code stinks! But we didn't have time to change the whole thing..
    mActiveObject = arg_obj;
    SceneObject *sceneObj = SceneManager::instance()->getSceneObjectByID(this->mActiveObject);
    if(dynamic_cast<VolumeObject*>(sceneObj))
    {
            Transform tmpTrans = SceneManager::instance()->getSceneObjectByID(this->mActiveObject)->getTransform();
            mTransform = new Transform(0);
            mTransform->setPosition(QVector3D(0.0f,0.0f,-6.0f));
            mTransform->setScale(tmpTrans.getScale());
            mTransform->setRotation(tmpTrans.getRotation());
            mVolumeTransform = mTransform;
    }
    else
        mTransform = &sceneObj->getTransform();

}

void VolumeRenderView::setVolumeObject(std::string arg_obj)
{
    //View::setVolumeObject(arg_obj);
    mVolumeObject = arg_obj;
    setActiveObject(arg_obj);
}


void VolumeRenderView::setGradientFilterRange(QVector2D arg_vec)
{
    mGradientFilterRange = arg_vec;
}

QVector2D VolumeRenderView::getGradientFilterRange()
{
    return mGradientFilterRange;
}

void VolumeRenderView::setMaximumIntensityProjection(bool arg_enabled)
{
    mMaximumIntensityProjection = arg_enabled;
}

bool VolumeRenderView::getMaximumIntensityProjection()
{
    return mMaximumIntensityProjection;
}
