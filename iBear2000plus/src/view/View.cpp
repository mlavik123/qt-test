#include "View.h"
#include "Viewport.h"
#include "debug.h"
#include "SceneManager.h"
#include "resource_manager.h"
#include "TransferFunctionResource.h"


void View::notify(Subject *arg_subject)
{

}

void View::setActiveObject(std::string arg_obj)
{
    LOG_INFO() << "NEI";
    SceneObject *sceneObject = SceneManager::instance()->getSceneObjectByID(mActiveObject);
    if(sceneObject)
        sceneObject->unregisterObserver(this);

    mActiveObject = arg_obj;

    sceneObject = SceneManager::instance()->getSceneObjectByID(mActiveObject);
    if(sceneObject)
        sceneObject->registerObserver(this);
    else
        LOG_ERROR() << arg_obj << " does not exist";

    this->setDirty(true);
}

void View::setVolumeObject(std::string arg_obj)
{
    mVolumeObject = arg_obj;
}

void View::setViewport(Viewport* arg_viewport)
{
    mViewport = arg_viewport;
}


void View::setTransferFunction(std::string arg_tf)
{
    this->mTransferFunction = arg_tf;
    TransferFunctionResource* res = ResourceManager<TransferFunctionResource>::instance()->getResource(mTransferFunction.c_str());
    if(!res)
        LOG_WARNING() << "transfer function resource " << res->getResourceName() << " does not exist";
    else
        res->getTransferFunction()->registerObserver(this);
}

std::string View::getTransferFunction()
{
    return this->mTransferFunction;
}


