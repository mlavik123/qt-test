#ifndef VOLUMERENDERVIEW_H
#define VOLUMERENDERVIEW_H

#include "GLIncludes.h"
#include <QOpenGLWidget>
#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <memory>

#include "View.h"
#include "VolumeData.h"
#include "Transform.h"
#include "TransferFunction.h"
#include "TransferFunctionResource.h"
#include "UIFactory.h"
#include "NoiseTexture1D.h"
#include "debug.h"
#include <qglframebufferobject.h>
#include "GlobalSettings.h"
#include <QTimer>

class QOpenGLTexture;
class QOpenGLShaderProgram;

class VolumeRenderView : public QOpenGLWidget, protected QOpenGLFunctions, public View
{
	Q_OBJECT

public:
    VolumeRenderView(QWidget *parent);
    ~VolumeRenderView();

    void notify(Subject *arg_subject) override;
    void setVolumeObject(std::string arg_obj) override;
    void setActiveObject(std::string arg_obj) override;
    void setGradientFilterRange(QVector2D arg_vec);
    QVector2D getGradientFilterRange();
    void setMaximumIntensityProjection(bool arg_enabled);
    bool getMaximumIntensityProjection();

protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override ;

    void keyPressEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *eventPress) override;
    void mouseReleaseEvent(QMouseEvent *eventPress) override;
    void mouseMoveEvent(QMouseEvent * event) override;
    void wheelEvent(QWheelEvent *event) override;

    void updateView() override ;




private:
    void makeObject();

    GLfloat fov = 65.0f;
    GLfloat mWidth;
    GLfloat mHeight;
    GLfloat mAspectRatio;

    std::shared_ptr<QOpenGLShaderProgram> mProgram;
    std::shared_ptr<QOpenGLShaderProgram> mGeometryProgram;
    std::shared_ptr<QOpenGLTexture> mTextures[6];
    QOpenGLBuffer mVbo;
    QOpenGLBuffer mPlaneVbo;

    NoiseTexture1D mNoiseTexture;
    QVector2D mMousePos;
    QGLFramebufferObject *texture_fbo;
    bool bIgnoreNextMouseMove;
    bool mShowGeometry;
    bool mMaximumIntensityProjection;
    QTimer *mTimer;
    QVector2D mGradientFilterRange;
    Transform *mTransform;  // active object's transform
    Transform *mVolumeTransform; // volume object's transform

    bool mNotified;

    void subscribeToSceneObjects();

public slots:
    void callbackAddPlane()
    {
        UIFactory::addPlane();
    }

    void callbackSetActiveObject()
    {
        UIFactory::selectActiveObject(this);
    }

    void callbackEditObjects()
    {
        UIFactory::editObjects();
    }

    void timerTimeout();

    void callbackChangeViewType()
    {
        UIFactory::changeViewType(mViewport);
    }

    void callbackShowHideGeometry()
    {
        mShowGeometry = !mShowGeometry;
    }

    void callbackLoadVolume()
    {
        UIFactory::loadVolume(this);
    }

    void callbackShowSettings()
    {
        UIFactory::showVolumeRendererSettings(this);
    }

    void callbackSetVolume()
    {
        UIFactory::setVolume(this);
    }

    void callbackSetActiveTF()
    {
        UIFactory::selectActiveTF(this);
    }


};

#endif // GLWIDGET_H
