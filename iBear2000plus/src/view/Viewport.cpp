//
// Created by Jostein Eriksen on 03/03/16.
//

#include "Viewport.h"
#include "EmptyView.h"



Viewport::Viewport(QWidget *parent, const char *name) : QWidget(parent)
{
    layout = new QGridLayout();
    this->setLayout (layout);

}

Viewport::~Viewport()
{

}

void Viewport::setViewportType(Viewport::ViewportType arg_type)
{
    mType = arg_type;

  // Main layout:
    QWidget *view;

  // Add View widget to layout
    switch(arg_type)
    {
        case Viewport::VOLUME:
            view = new VolumeRenderView(this);
            break;

		case Viewport::SLICE:
			view = new SliceRenderView(this);
			break;

        case Viewport::HISTOGRAM:
            view = new HistogramRenderView(this);
            break;
        case Viewport::EMPTY:
            view = new EmptyView(this);
            break;
        // TODO:
        //  sjekk andre Viewport typar
    }
    (dynamic_cast<View*>(view))->setViewport(this);
view->setFocusPolicy(Qt::FocusPolicy::ClickFocus);
  // Add view to layout
    if(view)
    {
        if(!layout->isEmpty())
            layout->takeAt((0));
        layout->addWidget(view);
    }
    else
        std::cout << "Undefined Viewport type in Viewport::setViewPortType(...)" << std::endl;
}
