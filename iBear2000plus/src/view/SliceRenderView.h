#ifndef SLICERENDERVIEW_H
#define SLICERENDERVIEW_H
#include "GLIncludes.h"
#include <qwidget.h>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLBuffer>
#include <QMatrix4x4>
#include <memory>
#include <QOpenGLShaderProgram>
#include <qopengltexture.h>
#include <QMouseEvent>

#include <src/scene/Transform.h>
#include <QTimer>
#include "View.h"
#include "VolumeData.h"
#include "TransferFunction.h"
#include <src/scene/VolumeObject.h>
#include "debug.h"
#include "UIFactory.h"



#define PROGRAM_VERTEX_ATTRIBUTE 0
#define PROGRAM_TEXCOORD_ATTRIBUTE 1

class SliceRenderView : public QOpenGLWidget, protected QOpenGLFunctions, public View
{
	Q_OBJECT

public:
	SliceRenderView(QWidget *parent);
	void notify(Subject *arg_subject) override { this->update(); };
    ~SliceRenderView();
protected:
    void initializeGL() override;
    void resizeGL(int w, int h) override;
    void paintGL() override ;
	void keyPressEvent(QKeyEvent *event) override;
	void mouseMoveEvent(QMouseEvent * event) override;
	void mousePressEvent(QMouseEvent * event) override;
    void mouseReleaseEvent(QMouseEvent * event) override;
    void wheelEvent(QWheelEvent *event) override;

    void updateView() override ;

private:
//	void makeObject();
	void initBuffers();

    float getValueAtMousePos(int arg_x, int arg_y);
    QVector3D getSliceToVolumeCoordinate(int arg_x, int arg_y);

	// mouse data
	QPoint mClickedPos;

	std::shared_ptr<QOpenGLShaderProgram> mShader;
//	QOpenGLBuffer mVbo;
	GLuint mVbo;
	GLuint mIbo;
	Transform* mTransformation;
	QMatrix4x4 mCombined;
	QMatrix4x4 mRotation;
//	QMatrix4x4 mMatrix;
	VolumeObject* mVolume;
	VolumeData* mVolumeData;
	GLuint mDataTexture;
	GLuint mGradientTexture;
    TransferFunction *tf;
    QTimer *mTimer;



	QVector3D mCamPosition = QVector3D(0.f, 0.f, 2.f);
	QVector3D mCamTarget = QVector3D(0.f, 0.f, -1.f);
	QVector3D mCamUp = QVector3D(0.f, 1.f, 0.f);
	float mCamFov = 30.f;
	float mCamAr = 1.f; // will be correctly initialized in the "display()" method
	float mCamZNear = 0.1f;
	float mCamZFar = 10000.f;
	float mCamZoom = 1.f;

    QMouseEvent *mMouseEvent;

//	QVector3D mEyePos;
//	GLfloat mFov = 45.0f;
//	GLfloat mEyeDistance = 6.0f;
public slots:
    void callbackAddKnot();
    void callbackSetHightlight();
    void timerTimeout();
    void callbackChangePlane();
    void callbackSetVolume();
    void callbackSetActiveTF();

};

#endif // GLWIDGET_H
