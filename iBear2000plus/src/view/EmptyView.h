#ifndef EMPTYVIEW_H
#define EMPTYVIEW_H
#include <qwidget.h>
#include "debug.h"
#include "View.h"
#include "UIFactory.h"


class EmptyView : public QWidget, public View
{
	Q_OBJECT

public:
    EmptyView(QWidget *parent);
    //void notify(Subject *arg_subject) override { };
    ~EmptyView();

protected:
	void mousePressEvent(QMouseEvent * event) override;

    //void updateView() override ;

public slots:
    void callbackChangeViewType()
    {
        UIFactory::changeViewType(mViewport);
    }


};

#endif // EMPTYVIEW_H
