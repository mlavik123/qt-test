#ifndef VOLUMEOBJECT_H
#define VOLUMEOBJECT_H

#include "SceneObject.h"
#include <string>
#include <VolumeData.h>
#include <QVector>

class VolumeObject : public SceneObject
{
public:
    VolumeObject(const char *arg_name, const char *arg_volume);
    ~VolumeObject();
    VolumeData &getVolumeData()
    {
        return mVolumeData;
    }

    QVector3D getHighlightPos();
    void setHightlightPos(QVector3D arg_pos);

private:
    VolumeData mVolumeData;
    QVector3D mHighlightPos;
};

#endif // VOLUMEOBJECT_H
