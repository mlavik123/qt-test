#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "singleton.h"
#include <unordered_map>

#include "type_traits"
#include "st_assert.h"
#include <vector>
#include <type_traits>
#include <typeinfo>
#include "subject.h"

class SceneObject; // forward declaration

class SceneManager : public Subject
{
    __DeclareSingleton(SceneManager)

public:
    SceneManager();
    ~SceneManager();

    SceneObject *getSceneObjectByName(std::string arg_name);
    SceneObject *getSceneObjectByID(std::string arg_id);
    SceneObject *getNextSceneObject(std::string arg_current);
    std::string addSceneObject(SceneObject* arg_object);

    // Gets a list of all SceneObjects of the given type in scene
    template <typename T>
    std::vector<T*> getSceneObjectsOfType()
    {
        std::vector<T*> objList;
        std::unordered_map<std::string, SceneObject*>::iterator it;
        it = mScene.begin();
        while(it != mScene.end())
        {
            //std::cout << typeid(it->second).name() << std::endl;
            if(dynamic_cast<T*>(it->second))
                objList.push_back(static_cast<T*>(it->second));
            it ++;
        }
        return objList;
    }

    template<typename T>
    T* getSceneObjectOfType()
    {
        std::unordered_map<std::string, SceneObject*>::iterator it;
        it = mScene.begin();
        while(it != mScene.end())
        {
            T* obj = dynamic_cast<T*>(it->second);
            if(obj != 0)
                return obj;
            it ++;
        }
        std::cout << "SceneManager has no objects of type: " << typeid(T).name() << std::endl;
        return 0;
    }

private:
    std::unordered_map<std::string, SceneObject*> mScene;



};

#endif // SCENEMANAGER_H
