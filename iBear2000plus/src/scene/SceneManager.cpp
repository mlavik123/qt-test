#include "SceneManager.h"

#include "SceneObject.h"
#include "debug.h"

__ImplementSingleton(SceneManager)

SceneManager::SceneManager(){
    __ConstructSingleton(SceneManager)
}

SceneManager::~SceneManager(){
    __DestructSingleton(SceneManager)
}

std::string SceneManager::addSceneObject(SceneObject* arg_object)
{
    mScene[arg_object->getMemoryHash()] = arg_object;

    notifyObservers();

    return arg_object->getMemoryHash();
}

SceneObject *SceneManager::getSceneObjectByID(std::string arg_id)
{
    try
    {
        return mScene.at(arg_id);
    } catch(std::out_of_range e)
    {
        LOG_WARNING() << "SceneManager has no SceneObject with hash ID: " << arg_id;
        return 0;
    }
}

SceneObject *SceneManager::getSceneObjectByName(std::string arg_name)
{
    std::unordered_map<std::string, SceneObject*>::iterator it = mScene.begin();
    while(it != mScene.end())
    {
        if(it->second->getName() == arg_name)
            return it->second;
        it++;
    }

    std::cout << "SceneManager has no SceneObject with name: " << arg_name << std::endl;
    return 0;
}

SceneObject *SceneManager::getNextSceneObject(std::string arg_current)
{
    std::unordered_map<std::string, SceneObject*>::iterator it;
    it = this->mScene.find(arg_current);
    if(it == mScene.end())
        return 0; // not found
    it++;
    if(it == mScene.end())
        it = mScene.begin();
    return it->second;
}
