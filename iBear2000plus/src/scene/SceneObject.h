#ifndef SCENEOBJECT_H
#define SCENEOBJECT_H

#include <string>
#include "Transform.h"
#include "subject.h"

class SceneObject : public Subject
{
private:
    std::string mName;
    Transform mTransform;

public:
    SceneObject(const char *arg_name);
    virtual ~SceneObject();
    Transform &getTransform();
    std::string getMemoryHash();
    std::string getName();
};

#endif // SCENEOBJECT_H
