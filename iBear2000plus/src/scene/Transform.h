#ifndef TRANSFORM_H
#define TRANSFORM_H


#include <QVector3D>
#include <QVector4D>
#include <QMatrix4x4>

class SceneObject;

class Transform{

public:
    Transform(SceneObject *arg_parent);
    ~Transform();

public:
    QVector3D getPosition();
    QMatrix4x4 getRotation();
    QMatrix4x4 getTranslationMatrix();
    QMatrix4x4 getScaleMatrix();
    QMatrix4x4 getModelMatrix();
	QVector3D getScale() { return mScale; }

    void setScale(QVector3D arg_scale);

    QVector3D up();
    QVector3D forward();
    QVector3D right();

    void roatate(float arg_angles, QVector3D arg_axis);
    void translate(QVector3D arg_trans);

    void setPosition(QVector3D arg_pos){mPosition = arg_pos;}
    void setRotation(QMatrix4x4 arg_rot){mRotation = arg_rot;}

public:
    QVector3D mPosition;

    QMatrix4x4 mRotation;
    SceneObject *mParent;

private:
	QVector3D mScale;
};

#endif // TRANSFORM_H
