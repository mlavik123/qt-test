#include "PlaneObject.h"

PlaneObject::PlaneObject(const char *arg_name) : SceneObject(arg_name)
{

}

PlaneObject::~PlaneObject()
{
}

QVector3D PlaneObject::getColour()
{
    return mColour;
}

void PlaneObject::setColour(QVector3D arg_colour)
{
    mColour = arg_colour;
}



