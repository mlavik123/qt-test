#include "VolumeObject.h"

VolumeObject::VolumeObject(const char *arg_name, const char *arg_volume) : SceneObject(arg_name)
{
    mVolumeData.readFile(arg_volume);
    getTransform().setScale(mVolumeData.getDistance());
    setHightlightPos(QVector3D(-1.0f,-1.0f,-1.0f));
}

VolumeObject::~VolumeObject()
{
}

QVector3D VolumeObject::getHighlightPos()
{
    return mHighlightPos;
}

void VolumeObject::setHightlightPos(QVector3D arg_pos)
{
    mHighlightPos = arg_pos;
}
