#include "SceneObject.h"

#include <sstream>
#include <iostream>

SceneObject::SceneObject(const char *arg_name) : mTransform(this)
{
    mName = arg_name;
}

SceneObject::~SceneObject()
{

}

Transform &SceneObject::getTransform()
{
    return mTransform;
}


std::string SceneObject::getMemoryHash()
{
    if(this == 0)
        std::cout << "getMemoryHash() called on NULL object" << std::endl;
    std::stringstream ss;
    ss << this;
    return ss.str();
}

std::string SceneObject::getName()
{
    return this->mName;
}


