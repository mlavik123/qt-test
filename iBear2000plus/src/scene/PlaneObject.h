#ifndef PLANEOBJECT_H
#define PLANEOBJECT_H

#include "SceneObject.h"
#include <qvector.h>

class PlaneObject : public SceneObject
{
public:
    PlaneObject(const char *arg_name);
    ~PlaneObject();
    bool mIsClippingPlane = false; // sett til "true" dersom den skal brukast som clipping plane i volume renderer


    QVector3D getColour();
    void setColour(QVector3D arg_colour);

private:
    QVector3D mColour;

};

#endif // PLANEOBJECT_H
