#include "Transform.h"

#include "SceneObject.h"

#include <iostream>

Transform::Transform(SceneObject *arg_parent)
{
    mParent = arg_parent;
    mScale = QVector3D(1.0f,1.0f,1.0f);
}

Transform::~Transform()
{

}

QVector3D Transform::getPosition()
{
    return mPosition;
}

QMatrix4x4 Transform::getRotation()
{
    return mRotation;
}

QMatrix4x4 Transform::getModelMatrix()
{
    return getTranslationMatrix()*mRotation*getScaleMatrix();
}

QMatrix4x4 Transform::getTranslationMatrix()
{
    return QMatrix4x4(1.0f,0.0f,0.0f,this->mPosition.x(),
                       0.0f,1.0f,0.0f,this->mPosition.y(),
                       0.0f,0.0f,1.0f,this->mPosition.z(),
                       0.0f,0.0f,0.0f,1.0f);
}

QMatrix4x4 Transform::getScaleMatrix()
{
    return QMatrix4x4( this->mScale.x(),0.0f,0.0f,0.0f,
                       0.0f,this->mScale.y(),0.0f,0.0f,
                       0.0f,0.0f,this->mScale.z(),0.0f,
                       0.0f,0.0f,0.0f,1.0f);
}

QVector3D Transform::up()
{
    return QVector3D(mRotation * QVector4D(0.0f,1.0f,0.0f,0.0f)).normalized();
}

QVector3D Transform::forward()
{
    return QVector3D(mRotation * QVector4D(0.0f,0.0f,-1.0f,0.0f)).normalized();
}

QVector3D Transform::right()
{
    return QVector3D(mRotation * QVector4D(1.0f,0.0f,0.0f,0.0f)).normalized();
}

void Transform::roatate(float arg_angles, QVector3D arg_axis)
{
    QMatrix4x4 mat;
    mat.rotate(arg_angles, arg_axis);
    mRotation = mat * mRotation;
    //mRotation.rotate(arg_angles, arg_axis);
    if(mParent)
        mParent->notifyObservers();
}

void Transform::translate(QVector3D arg_trans)
{
    this->mPosition += arg_trans;
    if(mParent)
        mParent->notifyObservers();
}





void Transform::setScale(QVector3D arg_scale)
{
    mScale = arg_scale;
}
