#ifndef OBSERVERMANAGER_H
#define OBSERVERMANAGER_H

class IObserver;
class Subject;

#include <unordered_map>
#include <string>

class ObserverManager
{
    static Subject mViewUpdater;
    static bool mViewIsDirty;

    struct SubjectObserverPair
    {
        Subject *subject;
        IObserver *observer;
        bool isDirty;
    public:
        SubjectObserverPair(IObserver *arg_observer, Subject *arg_subject)
        {
            subject = arg_subject;
            observer = arg_observer;
            isDirty = true;
        }
    };

public:
    void update();
    void setObserverDirty(IObserver *arg_observer, Subject *arg_subject);
    static ObserverManager* getInstance()
    {
        if(_instance == 0)
            _instance = new ObserverManager();
        return _instance;
    }

private:
    std::unordered_map<std::string,SubjectObserverPair*> mObservers;
    std::string getMemoryHash(SubjectObserverPair arg_pair);

    static ObserverManager* _instance;
};




#endif
