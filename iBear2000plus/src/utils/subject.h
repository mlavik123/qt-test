#ifndef SUBJECT_H
#define SUBJECT_H

#include "observer.h"

#include <vector>

class Subject
{
private:
    std::vector<IObserver*> mObserverCollection;
    bool mDirty;

public:
    void registerObserver(IObserver *arg_observer);
    void unregisterObserver(IObserver *arg_observer);
    void notifyObservers();

    std::vector<IObserver*> getObservers()
    {
        return mObserverCollection;
    }


};


#endif
