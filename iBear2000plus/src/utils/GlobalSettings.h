#ifndef GLOBALSETTINGS_H
#define GLOBALSETTINGS_H


class GlobalSettings
{

public:
    static int getRaymarchingSteps();
    static void setRaymarchingSteps(int arg_steps);

    static void setFrameBufferResDivisor(float arg_divisor);
    static float getFrameBufferResDivisor();

    static float frameres_high;
    static float frameres_low;

private:
    static int raymarchingSteps;
    static float frameBufferResDivisor;

};

#endif //GLOBALSETTINGS_H
