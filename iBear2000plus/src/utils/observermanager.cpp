#include "observermanager.h"

#include "observer.h"
#include <sstream>
#include <iterator>
#include <iostream>
#include "subject.h"
#include <vector>


ObserverManager* ObserverManager::_instance = 0;
bool ObserverManager::mViewIsDirty = false;

void ObserverManager::update()
{

    std::unordered_map<std::string,SubjectObserverPair*>::iterator it = mObservers.begin();
    while(it != mObservers.end())
    {
        if(it->second->observer == 0)
            std::cout << "Observer is NULL" << std::endl;

        if(it->second->isDirty)
            it->second->observer->notify(it->second->subject);
        it->second->isDirty = false;
        it++;
    }
}

void ObserverManager::setObserverDirty(IObserver *arg_observer, Subject *arg_subject)
{
    SubjectObserverPair* pair = mObservers[getMemoryHash(SubjectObserverPair(arg_observer, arg_subject))];
    if(!pair)
        mObservers[getMemoryHash(SubjectObserverPair(arg_observer, arg_subject))] = new SubjectObserverPair(arg_observer, arg_subject);
    else
        pair->isDirty = true;
}

std::string ObserverManager::getMemoryHash(SubjectObserverPair arg_pair)
{
    std::stringstream ss;
    ss << arg_pair.observer << arg_pair.subject;
    return ss.str();
}
