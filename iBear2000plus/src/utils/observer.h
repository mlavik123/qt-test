#ifndef OBSERVER_H
#define OBSERVER_H


class Subject;

class IObserver
{
public:
    virtual void notify(Subject *arg_subject);


    bool isDirty()
    {
        return mDirty;
    }
    void setDirty(bool arg_dirty)
    {
        mDirty = arg_dirty;
    }

private:
    bool mDirty;

};




#endif
