#ifndef ST_ASSERT_H
#define ST_ASSERT_H

#endif // ST_ASSERT_H

#include <assert.h>
#include <iostream>

#define __Assert(expr) \
    if(!(expr)) \
        std::cout << "Assertion error: " << #expr  << std::endl; \
    assert(expr);

#define __AssertComment(expr, comment) \
    if(!(expr)) \
        std::cout << "Assertion error: " << #expr << "\n" << comment << std::endl; \
    assert(expr);
