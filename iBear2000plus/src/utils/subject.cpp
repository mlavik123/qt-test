#include "subject.h"
#include "observermanager.h"

void Subject::registerObserver(IObserver *arg_observer)
{
    mObserverCollection.push_back(arg_observer);
}

void Subject::unregisterObserver(IObserver *arg_observer)
{

}

void Subject::notifyObservers()
{
    for(IObserver *currObserver : mObserverCollection)
    {
        ObserverManager::getInstance()->setObserverDirty(currObserver, this);
        //currObserver->notify(this);
    }
}
