#include "GlobalSettings.h"

int GlobalSettings::raymarchingSteps = 450;
float GlobalSettings::frameBufferResDivisor = 1.0f;

float GlobalSettings::frameres_high = 1.0f;
float GlobalSettings::frameres_low = 5.0f;



int GlobalSettings::getRaymarchingSteps()
{
    return raymarchingSteps;
}

void GlobalSettings::setRaymarchingSteps(int arg_steps)
{
    raymarchingSteps = arg_steps;
}

void GlobalSettings::setFrameBufferResDivisor(float arg_divisor)
{
    frameBufferResDivisor = arg_divisor;
}

float GlobalSettings::getFrameBufferResDivisor()
{
    return frameBufferResDivisor;
}
