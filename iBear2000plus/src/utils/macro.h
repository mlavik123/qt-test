#define SAFE_DELETE(var) if(var != 0) { delete var; var = 0; }
#define USHORT_MAX 65535
#define SHORT_MAX 32767