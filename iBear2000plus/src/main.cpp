#include "ibearmain.h"
#include <QApplication>
#include <QSplashScreen>
#include <QPixmap>


int main(int argc, char *argv[])
{



    QApplication a(argc, argv);

    QPixmap pixmap("data/splash.png");
    QSplashScreen splash(pixmap);
    splash.show();
    a.processEvents();

    IBearMain w;
    w.initialise();
    w.show();

    splash.finish(&w);

    while(!w.isClosed())
    {
        a.processEvents();
        w.update();
    }

    return 0;
    //return a.exec();
}
