#include "resource.h"

Resource::Resource(const char *arg_name, const char * arg_path)
{
    mResourceName = std::string(arg_name);
    mResourcePath = std::string(arg_path);
}

Resource::Resource(const char *arg_name)
{
    Resource(arg_name, arg_name);
}

Resource::~Resource()
{

}

void Resource::reload()
{

}

std::string Resource::getResourceName()
{
    return mResourceName;
}

std::string Resource::getResourcePath()
{
    return mResourcePath;
}
