//
// Created by Jostein Eriksen on 17/03/16.
//

#include "TransferFunctionResource.h"
#include "resource_manager.h"

void TransferFunctionResource::reload()
{

}

TransferFunctionResource::TransferFunctionResource(const char *name, TransferFunction *transferFunction) : Resource(name, name)
{
	mTf = transferFunction;
}

TransferFunction* TransferFunctionResource::getTransferFunction(const char* name) {
	TransferFunctionResource* transferFunctionResource = ResourceManager<TransferFunctionResource>::instance()->getResource(name);
	if(transferFunctionResource == nullptr) {
		transferFunctionResource = new TransferFunctionResource(name, TransferFunction::newFromPreset("default"));
		ResourceManager<TransferFunctionResource>::instance()->addResource(transferFunctionResource);
	}
	return transferFunctionResource->mTf;
}

TransferFunction *TransferFunctionResource::getTransferFunction()
{
    return this->mTf;
}
