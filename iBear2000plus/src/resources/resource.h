#ifndef RESOURCE_H
#define RESOURCE_H

#include <string>

class Resource
{

public:
    virtual void reload();

protected:
    std::string mResourceName;
    std::string mResourcePath;

public:
    Resource(const char *arg_name, const char * arg_path);
    Resource(const char *arg_name);
    ~Resource();

    std::string getResourceName();
    std::string getResourcePath();
};

#endif // RESOURCE_H