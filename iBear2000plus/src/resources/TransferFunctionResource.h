//
// Created by Jostein Eriksen on 17/03/16.
//

#ifndef IBEAR2000PLUS_TRANSFERFUNCTIONRESOURCE_H
#define IBEAR2000PLUS_TRANSFERFUNCTIONRESOURCE_H
#include "resource.h"
#include "TransferFunction.h"

class TransferFunctionResource : public Resource {
public:
	TransferFunctionResource(const char* name, TransferFunction* transferFunction);
	void reload() override;
    TransferFunction *getTransferFunction();

	static TransferFunction* getTransferFunction(const char* name);

private:
	TransferFunction*mTf;
};


#endif //IBEAR2000PLUS_TRANSFERFUNCTIONRESOURCE_H
