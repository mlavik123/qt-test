#ifndef IBEARMAIN_H
#define IBEARMAIN_H

#include <QMainWindow>

namespace Ui {
class IBearMain;
}

class IBearMain : public QMainWindow
{
    Q_OBJECT

public:
    explicit IBearMain(QWidget *parent = 0);
    ~IBearMain();
    void update();


    void closeEvent ( QCloseEvent * event ) override ;

    bool isClosed();

private:
    Ui::IBearMain *ui;
    bool mIsClosed = false;

public:
    void initialise();
};

#endif // IBEARMAIN_H
