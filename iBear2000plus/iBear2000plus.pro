#-------------------------------------------------
#
# Project created by QtCreator 2016-03-03T11:41:03
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

INCLUDEPATH += src/ui
INCLUDEPATH += src/resources
INCLUDEPATH += src/data
INCLUDEPATH += src/scene
INCLUDEPATH += src/utils
INCLUDEPATH += src/view
INCLUDEPATH += src
//INCLUDEPATH += src/ui

TARGET = iBear2000plus
TEMPLATE = app
CONFIG += c++11
QMAKE_LFLAGS += -v




FORMS    += src/ibearmain.ui

RESOURCES += \
    shaders.qrc

OTHER_FILES += \
    volrend_frag.fsh.glsl \
    volrend_vert.vsh.glsl \
    data/hand.dat

HEADERS += \
    src/data/NoiseTexture1D.h \
    src/data/TransferFunction.h \
    src/data/VolumeData.h \
    src/GL/glew.h \
    src/GL/glxew.h \
    src/GL/wglew.h \
    src/resources/resource.h \
    src/resources/resource_manager.h \
    src/resources/TransferFunctionResource.h \
    src/scene/PlaneObject.h \
    src/scene/SceneManager.h \
    src/scene/SceneObject.h \
    src/scene/Transform.h \
    src/scene/VolumeObject.h \
    src/ui/MovableButton.h \
    src/ui/qpiemenu.h \
    src/ui/UIFactory.h \
    src/utils/debug.h \
    src/utils/GLIncludes.h \
    src/utils/GlobalSettings.h \
    src/utils/observer.h \
    src/utils/observermanager.h \
    src/utils/singleton.h \
    src/utils/st_assert.h \
    src/utils/subject.h \
    src/utils/viewfactory.h \
    src/view/EmptyView.h \
    src/view/HistogramRenderView.h \
    src/view/SliceRenderView.h \
    src/view/View.h \
    src/view/Viewport.h \
    src/view/VolumeRenderView.h \
    ui_ibearmain.h

SOURCES += \
    src/data/NoiseTexture1D.cpp \
    src/data/TransferFunction.cpp \
    src/data/VolumeData.cpp \
    src/resources/resource.cpp \
    src/resources/resource_manager.cpp \
    src/resources/TransferFunctionResource.cpp \
    src/scene/PlaneObject.cpp \
    src/scene/SceneManager.cpp \
    src/scene/SceneObject.cpp \
    src/scene/Transform.cpp \
    src/scene/VolumeObject.cpp \
    src/ui/MovableButton.cpp \
    src/ui/qpiemenu.cpp \
    src/ui/UIFactory.cpp \
    src/utils/debug.cpp \
    src/utils/GLIncludes.cpp \
    src/utils/GlobalSettings.cpp \
    src/utils/observer.cpp \
    src/utils/observermanager.cpp \
    src/utils/singleton.cpp \
    src/utils/st_assert.cpp \
    src/utils/subject.cpp \
    src/utils/viewfactory.cpp \
    src/view/EmptyView.cpp \
    src/view/HistogramRenderView.cpp \
    src/view/SliceRenderView.cpp \
    src/view/View.cpp \
    src/view/Viewport.cpp \
    src/view/VolumeRenderView.cpp
