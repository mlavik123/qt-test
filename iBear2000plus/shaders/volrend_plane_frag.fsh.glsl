
uniform bool isClippingPlane;

varying vec3 vertPos;

void main()
{

    float alpha = 0.8;

    if(vertPos.y > -0.85 && vertPos.y < 0.85 && vertPos.x > -0.85 && vertPos.x < 0.85)
        alpha = 0.0;



    gl_FragColor = vec4(0.0,0.0,1.0,alpha);

}
