attribute highp vec4 vertex;

uniform mediump mat4 matrix;
uniform mediump mat4 projmat;

varying vec3 vertPos;


void main()
{
    vertPos = vertex.xyz;
    gl_Position = projmat * matrix * vertex;

}

