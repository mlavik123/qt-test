// Translation to apply to vertices (constant for every vertex)
uniform mat4 translationCombinedMatrix;

//uniform mat4 projectionMatrix;
uniform mat4 viewCombinedMatrix;

// The position of a vertex (per-vertex, from the VBO)
attribute vec4 position;

// Output vertex color (per-vertex, interpolated and passed to frag shader)
varying vec3 texCoord;
varying vec2 tex2dCoord;

void main() {
	// translate the vertex
	gl_Position = position;
	tex2dCoord = position.xy;
    texCoord = (translationCombinedMatrix * position).xyz;
}
