// ---------------------------
// ----- Volume Renderer -----
//----------------------------
// Raytracing is done in local (texture) space.
// A ray position and direction is calculated, such that you for each position along
//  the ray can easily get the texture coordinate, without performing any rotations etc.


varying mediump vec3 texc;
varying mediump vec3 fragCoord;
varying mediump mat4 viewmat;

struct Plane
{
    vec3 position;
    vec3 normal;
    bool clipping;
    mat4 tbn;
};

int max(int a, int b) { return a > b? a: b; }
float max(float a, float b) { return a > b? a: b; }

uniform sampler3D datatexture;
uniform sampler2D tftexture;
uniform sampler2D noisetexture;
uniform sampler3D gradientdir;
uniform int steps;
uniform vec3 screenRes;
uniform int numPlanes;
uniform Plane planes[10];
uniform float aspectRatio;
uniform vec3 scale;
uniform vec2 gradientMagRange;
uniform bool maximumIntensityProjection;
uniform vec3 highlightPos;


void main()
{
    // Calculate the view vector
    highp vec4 tmp = viewmat * vec4(0.0,0.0,0.0,1.0); // because camera does not move
    highp vec3 eye = tmp.xyz;
    highp vec3 rayDir = texc - tmp.xyz;
    highp vec3 rayPos = tmp.xyz;

    rayDir = vec3(gl_FragCoord.x/screenRes.x - 0.5, gl_FragCoord.y/screenRes.y - 0.5,-0.5/(tan((screenRes.z/2.0)*3.1415926535/180.0))); // screenRes.z = fov
    rayDir.x = rayDir.x * aspectRatio;
    rayDir = (viewmat * vec4(rayDir,0.0)).xyz;

    float distance = length(texc-rayPos);
    float dist = min(4.5, distance);  // TODO: finn ut lengste avstand i kube
    float tSteps = min(3.0, distance)/3.0; // reduce step count when distance is small
    float minSteps = float(steps/3);
    int steps2 = steps;//int( max(minSteps, tSteps*float(steps)) );
    float stepSize = dist / float(steps2);
    rayDir = normalize(rayDir);

    rayPos = tmp.xyz+rayDir*distance;
    rayDir = -rayDir;

    float maxDensity = 0.0;

    vec3 col = vec3(0.0,0.0,0.0);
    float alpha = 0.0;

    int i = 0;
    for(int iPlane = 0; iPlane < numPlanes; iPlane ++)
    {
        float denom = dot(planes[iPlane].normal, -rayDir);
        float t = dot(planes[iPlane].position - tmp.xyz, planes[iPlane].normal) / denom;
        if(t > 0.0)
        {
            vec3 hitpos = tmp.xyz - rayDir*t;
            vec4 locPos = planes[iPlane].tbn * vec4(hitpos-planes[iPlane].position, 1.0);

            if((locPos.y > -0.85 && locPos.y < 0.85 && locPos.x > -0.85 && locPos.x < 0.85))
                continue;

            if(abs(locPos.x) < 1.0 && abs(locPos.y) < 1.0 && abs(locPos.z) < 2.0)
            {
                rayPos = hitpos;
                i = max(int(t),i);
            }
        }
    }


    vec3 stepVector = rayDir*stepSize;

    // Create a small random offset in order to remove artifacts
    rayPos = rayPos + stepVector*texture2D(noisetexture, vec2(gl_FragCoord.x/screenRes.x, gl_FragCoord.y/screenRes.y)).r;

    for(; i < steps2; i++)
    {
        // Calculate texture coordinate
        vec3 index = ((vec3(1.0,1.0,1.0))*rayPos/2.0+vec3(0.5,0.5,0.5));
        rayPos = rayPos + stepVector;
        if(index.x <= 0.0 || index.x >= 1.0 || index.y <= 0.0 || index.y >= 1.0 || index.z <= 0.0 || index.z >= 1.0)
        {
            continue; // BREAK!!!
        }

        bool skip = false;
        // Clipping, by plane:
        for(int iPlane = 0; iPlane < numPlanes; iPlane++)
        {
            vec3 tmpRayPlaneDir = normalize(planes[iPlane].position - rayPos);
            if(planes[iPlane].clipping && dot(tmpRayPlaneDir, planes[iPlane].normal) < 0.0)
                skip = true;
        }
        if(skip)
            continue;


        float density = texture3D(datatexture, vec3(index.x, index.y, index.z)).x;

        if(density > maxDensity)
            maxDensity = density;

        vec4 colVec = texture2D(tftexture, vec2(density, 1));

        vec3 gradient = texture3D(gradientdir, vec3(index.x, index.y, index.z)).rgb;
        if(gradient.z < gradientMagRange.x || gradient.z > gradientMagRange.y)
            continue;

        vec3 lightDir = normalize(vec3(0.0,-0.3,0.9));
        float lightIntensity = 0.5*(1.0 - dot(normalize(gradient.rgb), lightDir));
        colVec.rgb = colVec.rgb *(0.5 + 0.5*smoothstep(0.0,1.0, lightIntensity));

        if(length((highlightPos - index)*scale) < 0.05)
            colVec = vec4(1.0,1.0,0.0,1.0);

        // add colour
        float a = colVec.a;
        col = a*colVec.rgb + (1.0-a)*col;
        alpha = a + (1.0-a)*alpha;
    }

// Maximum intensity colouring:
if(maximumIntensityProjection)
{
    float intensity = maxDensity;
    gl_FragColor = vec4(intensity, intensity, intensity, 1.0);
}
// Normal colouring:
else
    gl_FragColor = vec4(col.x, col.y, col.z, alpha);

}
