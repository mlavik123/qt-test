attribute highp vec4 vertex;
attribute highp vec4 texCoord;
varying mediump vec3 texc;
uniform mediump mat4 matrix;
uniform mediump mat4 projmat;
uniform mediump mat4 invmat;
uniform float eyeDistance;
//uniform vec3 scale;
varying mat4 viewmat;

varying mediump vec3 fragCoord;

void main()
{
    fragCoord = vec3(matrix * vertex);
    gl_Position = projmat * matrix * (vertex);
    //gl_Position = projmat * (matrix * (vertex*vec4(1.0,1.0,2.0,1.0)) + vec4(0.0,0.0,-eyeDistance,0.0));
    texc = texCoord.xyz;
    viewmat = invmat;
}

