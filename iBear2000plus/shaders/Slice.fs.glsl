varying mediump vec3 texCoord;
varying mediump vec2 tex2dCoord;
uniform sampler3D datatexture;
uniform sampler2D transferTexture;
uniform vec3 textureSize;


void main() {
	// Set the output color according to the input
	float density;
	if(texCoord.x <= 1. && texCoord.x >= -1.
	&& texCoord.y <= 1. && texCoord.y >= -1.
	&& texCoord.z <= 1. && texCoord.z >= -1.)
        density = texture3D(datatexture,   ((texCoord)/2.0)+vec3(0.5,0.5,0.5)    ).x;
    else
        density = 0.;

    vec4 col = texture2D(transferTexture, vec2(density, 1));
//    vec4 col = vec4(texture3D(datatexture,   ((texCoord)/2.0)+vec3(0.5,0.5,0.5)    ).aaaa);

    col.a = 1.0;
    gl_FragColor = col;
}
