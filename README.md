Visualisation project (for University course)

![alt tag](http://i66.tinypic.com/3wnsl.jpg)

# Build instructions:

## Windows
dependencies:

* MinGW: https://sourceforge.net/projects/mingw/files/
* CMake: https://cmake.org/
* QT: https://www.qt.io/download/
    * Install QT 5.6 or higher for MinGW 4.9.2 32 bit compiler.


After installing MinGW, Qt and CMake as normally you need to add an environment variable named CMAKE_PREFIX_PATH which points to your QT MinGW directory. Eg: CMAKE_PREFIX_PATH=C:\Qt\5.6\mingw49_32

1. Start Qt Creator
2. Click Open Project
3. Browse to the location you unzipped our project
4. Open the CMakeLists.txt file
5. In the Configure Project window choose: Desktop Qt 5.6.0 MingGW 32 bit and click configure project
6. CMake wizzard window should appear automatically. Make sure MinGW is selected as Generator then click Run Cmake
7. When done; click finish.
8. Click The HUGE green play icon in the bottom left corner to build and run the project.

## Linux
Dependencies:

* gcc
* cmake
* make
* Qt: https://www.qt.io/download/

Install the dependencies anyway you want. Open a terminal. Change directory to the project root and run the following commands.
NOTE: change the path to Qt to wherever you Qt installation is.

```
$ export CMAKE_PREFIX_PATH=/home/JohnDoe/Qt/5.5/gcc_64  
$ cmake .   
$ make  
$ cd build  
$ ./iBear2000Plus  
```

## OS X 
Dependencies:

- xcode
- cmake
- make
- Qt: https://www.qt.io/download/

Install the dependencies anyway you want. Open a terminal. Change directory to the project root and run the following commands.
NOTE: change the path to Qt to wherever you Qt installation is.

```
$ export CMAKE_PREFIX_PATH=/Users/JohnDoe/Applications/Qt/5.5/clang_64  
$ cmake .   
$ make  
$ cd build  
$ ./iBear2000Plus  
```